#!/bin/bash
#This script shoudl run under /bin/bash
if [ -z "$BASH_VERSION" ]; then
  echo "Please do ./$0"
  exit 1
fi

DIR_I="$HOME/spectroscopy"
BRANCH="main"
install_program () {
  #Maybe needed?
  #sudo apt-add-repository universe
  sudo apt-get install apt-transport-https ca-certificates -y
  sudo update-ca-certificates
  sudo apt-get update
  #Maybe needed?
  #sudo apt-get -y install software-properties-common
  sudo apt-get -y install git
  git clone https://gitlab.ethz.ch/elek/Praktikum/spectroscopy.git $DIR_I || git -C $DIR_I pull origin $BRANCH
  echo "---GIT Repository is updated"
  sudo apt-get -y install gitk
  sudo apt-get -y install git-gui

  sudo apt-get -y install python3-pip
  sudo apt-get -y install python3.10-venv
  sudo apt-get -y install python3-tk

  echo "---Make a virtual environment for Python"
  python3 -m venv $DIR_I/venv  #Make a virtual environment for the project
  echo "---Activate Virtual environment"
  #exec /bin/bash "source" "${DIR_I}/venv/bin/activate"
  source "${DIR_I}/venv/bin/activate"
  echo "---Install Dependencies"
  pip3 install pipreqs
  pip3 install -r $DIR_I/requirements.txt
  deactivate  #deactivate Virtual environment

  echo "---Copy Desktop Files"
  cp -r $DIR_I/THR1000.desktop ~/Desktop/
  echo "---Desktop Files copied"
  #echo "#!/bin/bash\npython3 ${DIR_I}/THR1000.py" | sudo tee /usr/local/bin/THR1000
  echo -e "#!/bin/bash\n${DIR_I}/venv/bin/python ${DIR_I}/THR1000.py" | sudo tee /usr/local/bin/THR1000

  sudo chmod 755 /usr/local/bin/THR1000
  echo "---Binary files Created in: /usr/local/bin"

  sudo usermod -a -G dialout $USER

  echo "---Installed Optional Tools"
  sudo apt-get -y install htop

  #install_RStudio
}

install_RStudio () {
  sudo apt-get -y install r-base
  #wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2022.07.1-554-amd64.deb
  #sudo dpkg -i rstudio-2022.07.1-554-amd64.deb

  wget https://s3.amazonaws.com/rstudio-ide-build/desktop/jammy/amd64/rstudio-2022.07.2-576-amd64.deb
  #sudo apt deb rstudio-2022.07.2-576-amd64.deb
  sudo dpkg -i rstudio-2022.07.2-576-amd64.deb
  ##If dependencies are missing,this will fix it and install
  sudo apt-get install -y -f
}

self_update_program () {
  git -C $DIR_I pull origin $BRANCH
  cp $DIR_I/install.sh $DIR_I/install_self.sh
  echo "---Copied Self installing script"
  echo "---RUN the self installing script..."
  #sh $DIR_I/install_self.sh -f
  . $DIR_I/install_self.sh -f
  rm -rf $DIR_I/install_self.sh
  echo "---:DONE Self Update:---"
}

delete_program () {
  sudo apt-get -y remove python3-pip
  sudo apt-get -y remove python3.10-venv
  sudo apt-get -y remove python3-tk
  sudo apt-get -y autoremove

  rm -rf $DIR_I

  sudo rm -rf /usr/local/bin/THR1000

  rm -rf ~/Desktop/THR1000.desktop
}

show_help () {
  echo "### INSTALL SCRIPT ###"
  echo "run: sh install.sh for default installation (without arguments)"
  echo "### OPTIONAL ARGUMENTS ###"
  echo " -f force install the program"
  echo " -d delete and uninstall program"
  echo " -u update the program"
  echo " -h help (Show this Help)"
}

#INPUT Parameters?
#echo "INPUT: $1";

if [ "$1" = '-d' ]; then
  ###-d remove and uninstall programs ###
  echo "DELETE PROGRAM FROM: ${DIR_I} Folder."
  delete_program
  echo "ALL REMOVED!"
elif [ "$1" = '-f' ]; then
  ###-f Force install program ###
  echo "Force Installing program in: ${DIR_I} Folder."
  install_program
  echo "---PROGRAM INSTALLED, ALL DONE!, Please Reboot!---"
elif [ "$1" = '-u' ]; then
  ###-u Update the program ###
  echo "SELF Updating the program in ${DIR_I} Folder"
  self_update_program
  echo "UPDATED, ALL DONE!"
elif [ "$1" = '-h' ]; then
  ###-h Help ###
  show_help
  echo "---ALL DONE!---"
else
  #NO INPUT Parameters given
  if [ -d "$DIR_I" ]; then
    ###if $DIR exists ###
    echo "---SELF Updating the program in ${DIR_I} Folder---"
    self_update_program
    #install_program
    echo "---UPDATED, ALL DONE!---"
  else
    ###if $DIR does NOT exist ###
    echo "Installing program in: ${DIR_I} Folder."
    install_program
    echo "---PROGRAM INSTALLED, ALL DONE!, Please Reboot!---"
  fi
fi

