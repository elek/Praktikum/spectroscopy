# Introduction
Spectroscopy is a software that allows to control the supported devices.
This tool allows to record, show and save the collected data. 
The GUI is created with _Qt Designer_.
# Linux Support 
- Mint 21 (cinnamon) 
- Kubuntu 22.04
- Xubuntu 20.04

# Software installation and update
Run the script:
```
$./install.sh
```
# Supported devices
- [THR1000](THR1000) monochromator (Jobin-Yvon)

# Hardware Requirements
This tool requires an additional USB interface, that receives commands

# Issues
- Issue with ttyUSB because of brltty software
```
$dmesg
 usbfs: interface 0 claimed by cp210x while 'brltty' sets config #1
$sudo apt-get remove brltty
```
- Issue accessing ttyUSB ports
```
sudo usermod -a -G dialout $USER
```

# For Developers
- Install pycharm

```
-  For Linux Mint: Snap package manager should be enabled by moving or deleting the following file:
$sudo mv /etc/apt/preferences.d/nosnap.pref ~/Documents/nosnap.backup
```
```
$sudo apt-get install snapd
$sudo snap install pycharm-community --classic
```
- To generate the gui_design.py after changes with _Qt Designer_
```
$pyuic5 -x gui_design.ui -o gui_design.py
#Requires pyqt5-dev-tools
$sudo apt-get install pyqt5-dev-tools
```
- Check the python dependencies, creating a **requirements.txt**
```
$cd spectroscopy
$pipreqs 
```
- This project uses pyqtgraph, there are some examples that we can try
```
$python3
>>>import pyqtgraph.examples
>>>pyqtgraph.examples.run()
```
