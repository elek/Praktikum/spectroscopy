
#ifndef Webinterface_h
#define Webinterface_h

static const char PROGMEM index_html[] = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>THR1000 Control</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
  html {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
  }
  h1 {
    font-size: 1.8rem;
    color: white;
  }
  h2{
    font-size: 1.5rem;
    font-weight: bold;
    color: #143642;
  }
  .topnav {
    overflow: hidden;
    background-color: #143642;
  }
  body {
    margin: 0;
  }
  .settings {
    float: left;
    width: 30%%;
    max-width: 400px;
    padding: 20px;
  }
  .graph {
    float: left;
    width: 70%%;
    background-color: #F8F7F9;;
    box-shadow: 2px 2px 12px 1px rgba(140,140,140,.5);
    padding-top:10px;
    padding-bottom:20px;
  }
  .button {
    padding: 10px 20px;
    font-size: 20px;
    text-align: center;
    outline: none;
    color: #fff;
    background-color: #0f8b8d;
    border: none;
    border-radius: 5px;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
   }
   /*.button:hover {background-color: #0f8b8d}*/
   .button:active {
     background-color: #0f8b8d;
     box-shadow: 2 2px #CDCDCD;
     transform: translateY(2px);
   }
   .state {
     font-size: 1.5rem;
     color:#8c8c8c;
     font-weight: bold;
   }
  </style>
<title>THR1000 Control</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="data:,">
</head>
<body>
  <div class="topnav">
    <h1>THR1000 Control</h1>
  </div>
  <div class="settings">
    <h2>Measurement</h2>
    <p>
      <div>Wavelength start</div>
      <div><input type="number" id="startwl" name="startwl" min="0" max="15000" step="0.1" value="2000">&nbsp<span id="UNIT">%UNIT%</span></div>
    </p>
    <p>
      <div>Wavelength Stop</div>
      <div><input type="number" id="stopwl" name="stopwl" min="0" max="15000" step="0.1" value="8000">&nbsp<span id="UNIT1">%UNIT1%</span></div>
    </p>
    <p>
      <div>Measurement intervall:</div>
      <div><input type="number" id="meas_intervall" name="meas_intervall" min="0" max="15000" step="0.1" value="100">&nbspms</div>
    </p>
    <p>
      <div>Resolution</div>
      <div><input type="number" id="resolution" name="resolution" min="0.001" max="100" step="0.001" value="1">&nbsp<span id="UNIT2">%UNIT2%</span></div>
    </p>
    <p>
      <div>Measurement duration:</div>
      <div class="state"><span id="MEASDUR">%MEASDUR%</span> secs</div>
    </p>
    <div><button id="butt_start" class="button">Start</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="butt_stop" class="button">Stop</button></div>
    <p><button id="butt_save" class="button">Save As File and Image</button></p>
    <p>STATUS:<span id="STATUS">%STATUS%</span></p>
    <p>DEBUG:<span id="DEBUG">%DEBUG%</span></p>
  </div>
  <div class="settings">
    <h2>Settings</h2>
    <p>
      <div>Actual Wavelength</div>
      <div class="state"><span id="CURRWL">%CURRWL%</span>&nbsp<span id="UNIT3">%UNIT3%</span></div>
    </p>
    <p>
      <div>Signal Voltage</div>
      <div class="state"><span id="CURRVOLT">%CURRVOLT%</span>&nbspV</div>
    </p>
    <p>
      <div>PMT Source:</div>
      <div><input type="range" oninput="updateSlider(this)" id="PM_LEVEL1" min="0" max="100" value=50 step="1" class="slider"><class="state">PM level=<span id="PM_LEVEL"></span> &percnt;</div>
    </p>
    <p><br></p>
    <p>
      <div>Set new Wavelength</div>
      <div><input type="number" id="setwl" name="setwl" min="1" max="5">&nbsp<span id="UNIT4">%UNIT4%</span>&nbsp&nbsp<input type="submit" id="butt_setwl" value="Go"></div>
    </p>
    <p>
      <div>Go to wavelength</div>
      <div><input type="number" id="gotowl" name="gotowl" min="0" max="1500">&nbsp<span id="UNIT5">%UNIT5%</span>&nbsp&nbsp<input type="submit" id="butt_gotowl" value="Go"></div>
    </p>
  </div>
  <div class="graph">
    <p class="state">state: <span id="LED_STATE">%LED_STATE%</span></p>
  </div>
<script>
  var gateway = `ws://${window.location.hostname}/ws`;
  var websocket;
  window.addEventListener('load', onLoad);
  function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage; // <-- add this line
  }
  function onOpen(event) {
    console.log('Connection opened');
    websocket.send("getValues");  // <--get initial values
  }
  function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
  }
  function onMessage(event) {
    var obj = JSON.parse(event.data);
    var keys = Object.keys(obj);
    console.log('onMessage event data:');
    console.log(event.data);
    for (var i = 0; i < keys.length; i++){
      var key = keys[i];
      document.getElementById(key).innerHTML = obj[key];
      if (key == 'PM_LEVEL'){
        document.getElementById('PM_LEVEL1').value = obj[key];
      }
      if (key == 'UNIT'){
        document.getElementById('UNIT1').innerHTML = obj[key];
        document.getElementById('UNIT2').innerHTML = obj[key];
        document.getElementById('UNIT3').innerHTML = obj[key];
        document.getElementById('UNIT4').innerHTML = obj[key];
        document.getElementById('UNIT5').innerHTML = obj[key];
      }
    }
    // <--document.getElementById('MEASDUR').innerHTML = obj.MEASDUR;
  }
  function onLoad(event) {
    initWebSocket();
    initButton();
  }
  function initButton() {
    document.getElementById('butt_save').addEventListener('click', save);
    document.getElementById('butt_start').addEventListener('click', start);
    document.getElementById('butt_stop').addEventListener('click', stop);
    document.getElementById('butt_setwl').addEventListener('click', setwl);
    document.getElementById('butt_gotowl').addEventListener('click', gotowl);
  }
  function save(){ websocket.send('save'); }
  function start(){ websocket.send('start='+document.getElementById('startwl').value.toString()+'>'+document.getElementById('stopwl').value.toString()+'>'+document.getElementById('meas_intervall').value.toString()+'>'+document.getElementById('resolution').value.toString()); }
  function stop(){ websocket.send('stop'); }
  function setwl(){ websocket.send('setwl='+document.getElementById('setwl').value.toString()); }
  function gotowl(){ websocket.send('gotowl='+document.getElementById('gotowl').value.toString()); }

  function updateSlider(element) {
    var pm_value = document.getElementById(element.id).value;
    document.getElementById("PM_LEVEL").innerHTML = pm_value;
    console.log('update slider element');
    // <--console.log(element);
    websocket.send("pm_level="+pm_value.toString());
}
</script>
</body>
</html>
)rawliteral";
//////////"

#endif