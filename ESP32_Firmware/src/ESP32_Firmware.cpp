#include <Arduino.h>
#include <WiFi.h>
// #include "esp_timer.h"
//#include <soc/soc.h>          // disable brownout problems
//#include <soc/rtc_cntl_reg.h> // disable brownout problems
// #include <esp_http_server.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h> // Include ArduinoJson Library
#include <FastAccelStepper.h>
#include <Preferences.h> //for EEPROM
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_ADS1X15.h>
#include <Webinterface.h>

// Replace with your network credentials
#define def_ssid "THR1000"
#define def_password "yourPassword"
String ssid = def_ssid;
String password = def_password;
const char *hostname = "THR1000";

String inputString = "";

bool ledState = 0;

#define I2C_SDA 21
#define I2C_SCL 22

//PINs for ADC
#define ADC_RDY_IN 35 // DATA READY if using External ADC
#define ADC0_IN 35  // If using Internal ADC

#define LED_STAT_OUT 5  //GREEN LED
#define PM_RESET_OUT 23 //SET D type Flip-Flop- to apply PMT voltage after OVERDRIVE

//PINs for MOTOR Signals
#define MOT_STEP_OUT 4
#define MOT_DIR_OUT 16
#define MOT_EN_OUT 17
#define QUARTER_STEP_OUT 2  //If enabled sets 1/4 motor step otherwise is 1/2 step

#define SW_DOWN_IN 19  //End Switch 0nm
#define SW_UP_IN 18   //End Switch 1500nm

#define DAC_PM_PIN 25 //photomultiplier DAC pin
#define DAC_OFFSET_PIN 26 //photomultiplier OFFSET pin
#define PM_OVERDRIVE_IN 34  //if PM signal is >10V
#define GAIN_OUT 27 //Pin Enable 16x OPAMP Gain
#define TK1_OUT 33  //Pin Enable filter SLOW  -> cutof frequency fc=100Hz
#define TK2_OUT 32  //Pin Enable filter FAST  -> cutof frequency fc=2kHz
#define TK1_FC "100Hz"
#define TK2_FC "2kHz"

//JTAG PINS
#define J_TDI_PIN 12
#define J_TCK_PIN 13
#define J_TMS_PIN 14
#define J_TDO_PIN 15

#define UNDERVOLTAGE_PIN  36  //SENSOR_VP  //1 when the +15V input voltage is lower than 5Volt
//#define SENSOR_VN 39

#define MIN_SPEED 1 // the parameter is steps/sec 
#define MAX_SPEED 10000 // the parameter is steps/sec
#define ACCEL 800 //steps/s²

// #define ANG_TO_STEP 50 //1Ångström to position (0.1nm) is 50 steps (Resolution 1Å/50steps)
#define ANG_TO_STEP 100 // 1Ångström (0.1nm) is 100 steps (microstep -> Half step) ( )
// #define ANG_TO_STEP 200  // 1Ångström (0.1nm) is 200 steps (microstep -> Quarter step)

#define POS_TO_NM 1 / (ANG_TO_STEP * 10) // Conversion from Position to nanometer
#define POS_TO_ANG 1 / ANG_TO_STEP       // Conversion from Position to Ångström
#define DEFAULT_WL 8000                  // Default Wavelength Position(Ångström)

#define SECS_PER_SAMPLE 0.01f //the ADC takes around 0.01secs to get a sample //actually setDataRate ADS1115_860SPS -860 samples per second

//#define POS_MIN 2000 * ANG_TO_STEP  // min position in steps
#define POS_MIN 0 * ANG_TO_STEP  // min position (in steps)
#define POS_MAX 15000 * ANG_TO_STEP // max position (in steps)

FastAccelStepperEngine engine = FastAccelStepperEngine();
FastAccelStepper *stepper = NULL;

volatile long save_pos = DEFAULT_WL * ANG_TO_STEP;

Adafruit_ADS1115 i2c_adc;
bool use_I2C_ADC = true;

Preferences EEPROM_Data;

String pin_status = ""; // Holds the status of the pin

// Define Queue handle
#define QUEUE_TIMEOUT 500/portTICK_PERIOD_MS  //total timeout is 500ms
#define MAX_JASON_MSG_SIZE 150  //150chars

QueueHandle_t QueueHandle;
const int QueueElementSize = 10;

StaticJsonDocument<MAX_JASON_MSG_SIZE> doc_queue; //Save space for queue
StaticJsonDocument<MAX_JASON_MSG_SIZE> docmsg;
//JsonObject object = doc_queue.to<JsonObject>();

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");


struct scan_values_t{
  long start_wl;
  long stop_wl;
  int speed = MIN_SPEED;
  unsigned int resolution = 1;
  int adc_sample_count = 2;  //Average points when using ADC
  long reads_done = 0;
  bool moving_to_start = false;
  bool scanning = false;
  bool stop = false;
};
scan_values_t run_scan;

struct pm_values_t{  //Photomultiplier voltage values for Socket C956-4
  int dac_value;
  int curr = 500; //just give an inittial value
  int min = 400;  // Minimum High voltage configurable
  int max = 900;  // Maximum High voltage configurable
  int curr_mvolt;
  int min_mvolt = 1300;  //(millivolt) PMT voltage -400V programming voltage: 4volt divided by 3(amplifier gain)
  float max_mvolt = 0;    //(millivolt)PMT voltage -900V programming voltage: 0volt divided by 3(amplifier gain)
  int min_dac_value =(int)(255*min_mvolt/3300);
  int max_dac_value =(int)(255*max_mvolt/3300);
  int dac_offset = 10;
  int dac_offset_min = 0;
  int dac_offset_max = 255;
};
pm_values_t pmt;

#define BLINK_SLOW 500  //0.5s Blink
#define BLINK_FAST 200  //0.2s Blink

unsigned long time_millis = millis();

struct str_flags_t{
  String sw_down_name = "MIN Wavelength;";
  volatile bool sw_down = false;
  bool sw_down_handled = false;
  String sw_up_name = "MAX Wavelength;";
  volatile bool sw_up = false;
  bool sw_up_handled = false;
  String undervoltage_name = "Input Supply Fail;";
  volatile bool undervoltage = false;
  bool undervoltage_handled = false;
  String pm_overdrive_name = "PMT Overdrive;";
  volatile bool pm_overdrive = false;
  bool timeout_running = false;
  int pm_timeout = 3000;
  unsigned long pm_time_millis;
  bool gain_ON = false;
  bool gain_auto_mode = true;
  volatile bool new_ADC_data = false;
};
//str_flags_t flags;

struct state_machine_t {
  bool update = false;
  str_flags_t flags;
  bool debug = false;
  bool wifistart = false;
  bool led_stat_on = false;
  int led_blink_time = BLINK_FAST;
};
state_machine_t state;

int get_resolution (int speed){
  // the speed is in steps/sec (between MIN_SPEED(1) and MAX_SPEED(10 000))
  if (speed > 1000){
    run_scan.adc_sample_count = 2; //take 2 ADC samples per step 
    return 10*((int)(speed*SECS_PER_SAMPLE)/10); //steps/sample, divide per 10 to truncate decimals and multiply again
  }
  else if (speed >= 100){
    run_scan.adc_sample_count = 2; //take 2 ADC samples per step 
    return (int)(speed*SECS_PER_SAMPLE); //steps/sample
  }
  else{
    run_scan.adc_sample_count = 200/speed; //number of ADC samples per step (min is 2 samples, max is 200 samples) 
    return 1; //minimal resolution
    }
}

String get_flag_string (state_machine_t* sta){
  String buff = "";
  if (sta->flags.sw_down)
  {
    buff += String(sta->flags.sw_down_name);
  }
  if (sta->flags.sw_up)
  {
    buff += String(sta->flags.sw_up_name);
  }
  if (sta->flags.pm_overdrive)
  {
    buff += String(sta->flags.pm_overdrive_name);
  }
  if (sta->flags.undervoltage)
  {
    buff += String(sta->flags.undervoltage_name);
  }
  if (buff == "")
  {
    return String("OK");
  }
  else{
    return buff;
  }
}

void IRAM_ATTR isr_ADC_data_ready() {
  state.flags.new_ADC_data = true;
}

void IRAM_ATTR isr_pm_overdrive()
{ // PIN interrupt
  state.flags.pm_overdrive = true;
  if (state.debug){Serial.println("ERROR: "+state.flags.pm_overdrive_name);}
}
void IRAM_ATTR isr_sw_up()
{ // PIN interrupt
  state.flags.sw_up = true;
  if (state.debug){Serial.println("ERROR: "+state.flags.sw_up_name);}
}
void IRAM_ATTR isr_sw_down()
{ // PIN interrupt
  state.flags.sw_down = true;
  if (state.debug){Serial.println("ERROR: "+state.flags.sw_down_name);}
}

void IRAM_ATTR isr_undervoltage ()  //detects an undevoltage event
{
  if (digitalRead(UNDERVOLTAGE_PIN)) //verifies if the PIN is really on ON State
  {
    state.flags.undervoltage = true;
    if (state.debug){Serial.println("ERROR: "+state.flags.undervoltage_name);}
  }
}

float get_signal(void)
{
  float volts;
  if (use_I2C_ADC)
  {
    int32_t adc0=0;
    for (int i = 0; i < run_scan.adc_sample_count; i++)
    {
      while(!state.flags.new_ADC_data)  // waits for the interrupt with new ADC value
      {
        //if (state.debug){Serial.println("wait_100us");}
        delayMicroseconds(100); //new sample takes around (1/860)secs to arrive  
       //delay(1); //wait
      }
      adc0 += i2c_adc.getLastConversionResults();
    }
    adc0=adc0/run_scan.adc_sample_count;
    volts = i2c_adc.computeVolts((int16_t)adc0);
    volts = volts*5.0f; // compensate for the input voltage divider
    if (state.flags.gain_ON) //if the hardware gain is enabled
    {
      volts = volts/16.0f;  //input voltage is calculated dividing by opamp hardware gain (16x) 
      if(!run_scan.scanning) //if is not scanning we can use the AUTO MODE 
      {
        if (state.flags.gain_auto_mode) //if  GAin is in auto_mode
        {
          if (volts < 0.1f ) //if the adc voltage is < 0.1*16/5=0.32V
          {
            if(i2c_adc.getGain() != GAIN_EIGHT)
            {
              i2c_adc.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
              i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
              if (state.debug){Serial.println("Set GAIN_EIGHT +/- 0.512V");}
            }
          }
          else if (volts < 0.3f) //if the adc voltage is < 0.3*16/5=0.96V
          {
            if (i2c_adc.getGain() != GAIN_FOUR)
            {
            i2c_adc.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
            i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
            if (state.debug){Serial.println("Set GAIN_FOUR +/- 1.024V");}
            }
          }
          else if (volts < 0.6f)
          {
            if (i2c_adc.getGain() != GAIN_TWO)
            {
            i2c_adc.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
            i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
            if (state.debug){Serial.println("Set GAIN_TWO +/- 2.048V");}
            }
          }
          else //if amplified(16x) signal is too high (0.6*16)=9.6V (the max ADC voltage is 9.6/5=1.92)
          {
            state.flags.gain_ON = false; //Disable 16x OPAMP Gain
            digitalWrite(GAIN_OUT, state.flags.gain_ON); 
            delay(20); // wait so that the Filter OPAMP can be stable
            if (state.debug){Serial.println("Disable 16x OPAMP Gain");}
          }
        }
      }
    }
    else  //if the gain is disabled
    {
      if(!run_scan.scanning) //if is not scanning we can use the AUTO MODE 
      {
        if (volts < 0.5f && state.flags.gain_auto_mode) //if signal is too low <0.5Volt and GAin is in auto_mode
        {
          state.flags.gain_ON = true;//Enable 16x OPAMP Gain
          digitalWrite(GAIN_OUT, state.flags.gain_ON); 
          delay(20); // wait so that the Filter OPAMP can be stable
          if (state.debug){Serial.println("Enable 16x OPAMP Gain");}
        }
      }
    }
    state.flags.new_ADC_data = false;
  }
  else //If using the internal ADC
  {
    int adc_val = 0;
    for (unsigned int i = 0; i < run_scan.adc_sample_count; i++)
    { // measure values for averaging
      adc_val += analogRead(ADC0_IN);
    }
    volts = float(adc_val) / run_scan.adc_sample_count;
    //Serial.println("volts=" + String(volts,5));
    volts = volts / 1241.0; //(1241=4096/3.3volt)
  }
  return volts;
}

void task_gotowl(void *parameter)
{
  bool send_one_last_time = false;
  while (!send_one_last_time)
  {
    StaticJsonDocument<MAX_JASON_MSG_SIZE> docwl;
    JsonObject object = docwl.to<JsonObject>();
    if (!stepper->isRunning())
    { // if motor not running anymore it should just update one last time
      send_one_last_time = true;
      object["DEBUG"] = "Moving Done";
    }
    long pos = stepper->getCurrentPosition();
    object["CURRWL"] = String((float)pos * POS_TO_ANG);
    xQueueSend(QueueHandle, (void*) &docwl, QUEUE_TIMEOUT); //send message to queue
    delay(500); // wait
  }
  vTaskDelete(NULL);
}

void loopWebClients(void *parameter)
{
  while (true) 
  {
    JsonObject object = doc_queue.to<JsonObject>();
    int ret = xQueueReceive(QueueHandle, &doc_queue,QUEUE_TIMEOUT);  //get messeges from queue waitting 500ms for a message
    String jsonString = "";     // Temporary storage for the JSON String
    if(ret == pdPASS){
      serializeJson(doc_queue, jsonString); // collect the string
      if (state.debug){Serial.println("broadcast string" + jsonString);}
      ws.textAll(jsonString); // send the string to all clients
    }
    else{
      delay(200); // waits 100ms
    }
    if(ws.count()>=1)  // if there is at least 1 client
    {    
      StaticJsonDocument<MAX_JASON_MSG_SIZE> doc_volt;
      JsonObject object_volt = doc_volt.to<JsonObject>();
      object_volt["STATUS"] = get_flag_string (&state);
      object_volt["CURRVOLT"] = String(get_signal(),5); //TODO: possible conflict with get_signal() called from terminal
      object_volt["PM_LEVEL"] = pmt.curr;
      jsonString = "";
      serializeJson(doc_volt, jsonString); // collect the string
      //Serial.println("broadcast string" + jsonString);
      ws.textAll(jsonString); // send the string to all clients
    }
  }
  vTaskDelete(NULL);
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len)
{
  AwsFrameInfo *info = (AwsFrameInfo *)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT)
  {
    data[len] = 0;
    String message = (char *)data;
    JsonObject object = docmsg.to<JsonObject>();
    if (state.debug){Serial.println("Socket msg received:" + message);}
    if (strcmp((char *)data, "getValues") == 0)
    {
      object["UNIT"] = "Å";
      object["MEASDUR"] = pin_status;
      object["PM_LEVEL"] = pmt.curr;
      object["STATUS"] = get_flag_string(&state);
      object["DEBUG"] = "Starting";
      long pos = stepper->getCurrentPosition();
      object["CURRWL"] = String((float)pos * POS_TO_ANG);
      object["CURRVOLT"] = "0.00";
      object["LED_STATE"] = String(ledState);
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
    }
    if (strcmp((char *)data, "save") == 0)
    {
      ledState = !ledState;
      if (ledState)
      {
        pin_status = "LIGADO";
      }
      else
      {
        pin_status = "Desligado";
      }
      object["LED_STATE"] = String(ledState);
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
    }
    if (message.startsWith(("gotowl=")))
    {
      float set_pos_f = message.substring(7).toFloat();
      long set_pos = round(set_pos_f * ANG_TO_STEP);
      if (set_pos < POS_MIN || set_pos > POS_MAX)
      {
        object["DEBUG"] = "Invalid value in: Go To Wavelength";
        xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
      }
      else
      {
        stepper->moveTo(set_pos); // Move motor to set position
        object["DEBUG"] = "Moving to:" + String((float)set_pos * POS_TO_ANG) + "Å";
        xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
        //Serial.println("gowl=" + String((float)set_pos * POS_TO_ANG));
        ///* Function, name of the task,stack size,input_param, prority, task handle, Core
        xTaskCreatePinnedToCore(task_gotowl, "task_gotowl", 2048, NULL, 3, NULL, 1); // task to core
      }
    }
    if (message.startsWith(("setwl=")))
    {
    float set_pos_f = message.substring(6).toFloat();
    long set_pos = round(set_pos_f * ANG_TO_STEP);
    if (set_pos < POS_MIN || set_pos > POS_MAX)
    {
      object["DEBUG"] = "Invalid value in: Set new Wavelength";
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
    }
    else
    {
      stepper->setCurrentPosition(set_pos);
      object["DEBUG"] = "Set new Wavelength:" + String((float)set_pos * POS_TO_ANG) + "Å";
      long pos = stepper->getCurrentPosition();
      object["CURRWL"] = String((float)pos * POS_TO_ANG);
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
      //Serial.println("setwl=" + String((float)set_pos * POS_TO_ANG));
    }
    }
    if (message.startsWith(("start=")))
    {
      float start_wl = message.substring(6).toFloat();   
      int index = message.substring(6).indexOf(">");   //looks for the index where the next ">" separator is
      float stop_wl = message.substring(++index).toFloat();
      index = message.substring(index).indexOf(">");   //looks for the index where the next ">" separator is
      float interval = message.substring(++index).toFloat();
      index = message.substring(index).indexOf(">");   //looks for the index where the next ">" separator is
      float resolution = message.substring(++index).toFloat();
      object["DEBUG"] = "Starting new Measurement";
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
    }
    if (message.startsWith(("pm_level=")))
    {
      pmt.curr = message.substring(9).toInt();
      pmt.curr_mvolt = map(pmt.curr, pmt.min, pmt.max, pmt.min_mvolt, pmt.max_mvolt);
      pmt.dac_value = map(pmt.curr_mvolt, pmt.min_mvolt, pmt.max_mvolt, pmt.min_dac_value, pmt.max_dac_value);
      dacWrite(DAC_PM_PIN, pmt.dac_value);
    }
    if (strcmp((char *)data, "stop") == 0)
    { // Stop the motor
      run_scan.stop = true;
      stepper->stopMove();
      object["DEBUG"] = "Stopping";
      xQueueSend(QueueHandle, (void*) &docmsg, QUEUE_TIMEOUT); //send message to queue
    }
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  switch (type)
  {
  case WS_EVT_CONNECT:
    if (state.debug){Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());}
    if(ws.count()==1){ //if is the first connection trigger
    if (state.debug){Serial.println("First client connected");}
    }
    break;
  case WS_EVT_DISCONNECT:
    if (state.debug){Serial.printf("WebSocket client #%u disconnected\n", client->id());}
    break;
  case WS_EVT_DATA:
    handleWebSocketMessage(arg, data, len);
    break;
  case WS_EVT_PONG:
  case WS_EVT_ERROR:
    break;
  }
}

void initWebSocket()
{
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

/* ## Startup Screen for Serial Port  ## */
void print_version(void)
{
  Serial.println(F("STEP MOTOR Controller THR1000 V1.0"));
}

/* ## Show help to for Serial Port  ## */
void print_help()
{
  Serial.println(F("### Usage ###"));
  Serial.println(F("help -> Show this help"));
  Serial.println(F("version -> Show software version"));
  Serial.println(F("status -> Check device status"));
  Serial.println(F("ssid=yourssid -> Set Wifi SSID"));
  Serial.println(F("pass=yourpass -> Set Wifi password"));
  Serial.println(F("getIP -> Get Wifi IP"));
  Serial.println(F("startwifi=1 -> Start Wifi Module: 1-Enable, 0-Disable"));
  Serial.println(F("memclear -> Clear saved data"));
  Serial.println(F("reboot -> Reboot device"));
  Serial.println(F("gowl=8300 -> Move To Wavelength (Ångström) [2000Å - 15000Å]"));
  Serial.println(F("setwl=8000 -> Set Actual Wavelength (Ångström) [2000Å - 15000Å]"));
  Serial.println(F("scan=2000;2500;70 -> Make a scan form 2000Å to 2500Å, with speed 70Å/s, replies with voltage and wl"));
  Serial.println(F("stop -> stop the motor"));
  Serial.println(F("savewl -> Save actual Wavelength into memory"));
  Serial.println(F("getwl -> Get the actual Wavelength (Ångström)"));
  Serial.println(F("getspeed -> Get the actual speed (steps/second)"));
  Serial.println(F("get_mot_state -> Get the state of the motor (stop) or (run)"));
  Serial.println(F("getvolt -> Get the actual Voltage (Volt)"));
  Serial.println(F("getpmt -> Get Photomultiplier control voltage (Volt)"));
  Serial.println(F("getpmt_min -> Get Photomultiplier MIN control voltage (Volt)"));
  Serial.println(F("getpmt_max -> Get Photomultiplier MAX control voltage (Volt)"));
  Serial.println(F("setpmt=500 -> Set the Photomultiplier control voltage (Volt)"));
  Serial.println(F("getmsm_dur=2000;2500;10;0.1 -> Get measurement duration"));
  Serial.println(F("get_res=70 -> Get measurement Resolution for given scan speed ex:70Å/s"));
  Serial.println(F("get_amp_mode -> Get the Amplifier mode"));
  Serial.println(F("set_amp_mode=AUTO -> Set the Amplifier mode(AUTO, 1x, 16x, 32x or 64x)"));
  Serial.println(F("set_filter_mode=SLOW -> Set Filter mode (OFF, SLOW, FAST)"));
  Serial.println(F("debug=1 -> Set debug: 1-Enable, 0-Disable"));
}

void state_handler (void)
{
  if(state.flags.pm_overdrive)
  {
    if (state.flags.timeout_running)
    {
      time_millis = millis();
      if (time_millis > state.flags.pm_time_millis+state.flags.pm_timeout) //check timeout to apply PMT signal again
      {
        digitalWrite(PM_RESET_OUT,0);
        delay(1);
        digitalWrite(PM_RESET_OUT,1);
        delay(1);
        digitalWrite(PM_RESET_OUT,0);
        if(!digitalRead(PM_OVERDRIVE_IN)) // the PM_OVERDRIVE PIN was released
        {
          state.flags.pm_overdrive = false;
          state.flags.timeout_running = false;
        }
      }
    }
    else{
      state.flags.pm_time_millis = millis();
      state.flags.timeout_running = true;
    }
  }

  if(state.flags.sw_down)  //if switch of low wavelenght is activated 
  {
    if(digitalRead(SW_DOWN_IN)) // the switch was released
    {
      state.flags.sw_down = false;
      state.flags.sw_down_handled = false;
    }
    else //If the switch still activated
    {
      if (!state.flags.sw_down_handled) //if this state is not handled yet
      {
        run_scan.stop = true;  //scan should stop
        stepper->stopMove();  //motor should stop
        state.flags.sw_down_handled = true;
      }
    }
  }
  if(state.flags.sw_up)
  {
    if(digitalRead(SW_UP_IN)) // the switch was released
    {
      state.flags.sw_up = false;
      state.flags.sw_up_handled = false;
    }
    else //If the switch still activated
    {
      if (!state.flags.sw_up_handled) //if this state is not handled yet
      {
        run_scan.stop = true;  //scan should stop
        stepper->stopMove();  //motor should stop
        state.flags.sw_up_handled = true;
      }
    }
  }
  if(state.flags.undervoltage)
  {
    if (!state.flags.undervoltage_handled) //event not handled yet
    {
      //digitalWrite(QUARTER_STEP_OUT, 1); //Use 1/2 step
      save_pos = stepper->getCurrentPosition(); // get the current position to save
      EEPROM_Data.begin("my-app", false);
      EEPROM_Data.putLong("save_pos", save_pos);
      EEPROM_Data.end(); // Close the Preferences
      //digitalWrite(QUARTER_STEP_OUT, 0); //Use 1/2 step
      Serial.println("BACKUP: savewl=" + String((float)save_pos * POS_TO_ANG));
      state.flags.undervoltage_handled = true;
    }
    else{
      if(!digitalRead(UNDERVOLTAGE_PIN)) // the Undervoltage is gone
      {
        state.flags.undervoltage_handled = false;
        state.flags.undervoltage = false;
      }
    }
  }
  //get_flag_string(&state);
}

void scan_task(void *pvParameters)
{
  run_scan.scanning = true;
  run_scan.reads_done = 0;
  run_scan.stop = false;
  Serial.println("scan=starting=" + String(run_scan.start_wl)+","+String(run_scan.stop_wl)+","+String(run_scan.speed));
  stepper->moveTo(run_scan.start_wl); // Move motor to start position
  long pos = stepper->getCurrentPosition();
  run_scan.moving_to_start=true;
  while (stepper->isRunning())
  {
    pos = stepper->getCurrentPosition();
    String reply = "scan=moving_to_start=;wl=" + String((float)pos * POS_TO_ANG) + "=;volt=" + String(get_signal(),5)+"=G";
    Serial.println(reply); // print String
    delay(500); //wait that motor stops
  }
  run_scan.moving_to_start = false;
  if (run_scan.stop){  // if stop was requested
    run_scan.reads_done = 0;
    Serial.println("scan=reads_done="+String(run_scan.reads_done));
    run_scan.scanning = false;
    vTaskDelete(NULL);  // end of the task
  }
  bool send_one_last_time = false;
  bool is_running = true;
  Serial.println("scan=scanning");
  unsigned long i = 0;
  int resolution = get_resolution(run_scan.speed);
  pos = stepper->getCurrentPosition();
  long check_pos = pos;
  stepper->setSpeedInHz(run_scan.speed); // the parameter is steps/sec
  stepper->moveTo(run_scan.stop_wl); // Moving the motor to target stop_wl

  while (is_running || send_one_last_time)  //motor running
  {
    pos = stepper->getCurrentPosition();
    is_running = stepper->isRunning();
    if (state.debug){Serial.println(i);}
    if (pos >= check_pos)  //maybe there is a small error
    {
      i++;
      String reply = "scans=" + String(i) + "=;wl=" + String((float)check_pos * POS_TO_ANG) + "=;volt=" + String(get_signal(),5)+"=G";
      Serial.println(reply); // print String
      check_pos += resolution;
    }
    else{
      delay(1); //wait
    }
    if (!is_running)  //if the motor stopped, run one last time
    {
      if(!send_one_last_time){ send_one_last_time = true;}
      else{ send_one_last_time = false; }
    }
  }
  run_scan.reads_done = i;
  Serial.println("scan=reads_done="+String(run_scan.reads_done));
  run_scan.scanning = false;
  stepper->setSpeedInHz(MAX_SPEED); // the parameter is steps/sec
  vTaskDelete(NULL); // end of the task
}

/* ## The new command is processed here ## */
void Process_Command(void)
{
  // Serial.println("");
  if (inputString.equalsIgnoreCase("version"))
  {
    print_version();
  }
  else if (inputString.equalsIgnoreCase("help"))
  {
    print_help();
  }
  else if (inputString.equalsIgnoreCase("status"))
  {
    Serial.println("status="+get_flag_string(&state));
  }
  else if (inputString.startsWith("ssid="))
  {
    Serial.println(F("command=OK"));
    ssid = inputString.substring(5); // read the substring starting in index 5
    EEPROM_Data.begin("my-app", false);
    EEPROM_Data.putString("ssid", ssid);
    EEPROM_Data.end(); // Close the Preferences
  }
  else if (inputString.startsWith("pass="))
  {
    Serial.println(F("command=OK"));
    password = inputString.substring(5); // read the substring starting in index 5
    EEPROM_Data.begin("my-app", false);
    EEPROM_Data.putString("password", password);
    EEPROM_Data.end(); // Close the Preferences
  }
  else if (inputString.equalsIgnoreCase("getIP"))
  {
    Serial.println("getIP=" + WiFi.localIP());
  }
  else if (inputString.startsWith("startwifi="))
  {
    int index = inputString.indexOf('=');
    if(inputString.substring(index+1).toInt())  //0 or 1
    {
      state.wifistart = true;
    }
    else{
      state.wifistart = false;
    }
    EEPROM_Data.begin("my-app", false);
    EEPROM_Data.putBool("wifistart",state.wifistart);
    EEPROM_Data.end(); // Close the Preferences
    Serial.println("The Changes will take effect on next reboot");
    Serial.println("startwifi=" + String(state.wifistart));
  }
  else if (inputString.equalsIgnoreCase("memclear"))
  {
    Serial.println(F("command=OK"));
    EEPROM_Data.begin("my-app", false);
    EEPROM_Data.clear(); // clear data in mem ()
    EEPROM_Data.end();   // Close the Preferences
    Serial.println(F("All Cleared"));
  }
  else if (inputString.startsWith("setwl="))
  {
    float set_pos_f = inputString.substring(6).toFloat();
    long set_pos = round(set_pos_f * ANG_TO_STEP);
    if (set_pos < POS_MIN || set_pos > POS_MAX)
    {
      Serial.println("ERROR: Wavelength MIN is:" + String((float)POS_MIN * POS_TO_ANG) + " and Max is:" + String((float)POS_MAX * POS_TO_ANG));
    }
    else
    {
      stepper->setCurrentPosition(set_pos);
      Serial.println("setwl=" + String((float)set_pos * POS_TO_ANG));
    }
  }
  else if (inputString.startsWith("gowl="))
  {
    float set_pos_f = inputString.substring(5).toFloat();
    long set_pos = round(set_pos_f * ANG_TO_STEP);
    if (set_pos < POS_MIN || set_pos > POS_MAX)
    {
      Serial.println("ERROR: Wavelength MIN is:" + String((float)POS_MIN * POS_TO_ANG) + " and Max is:" + String((float)POS_MAX * POS_TO_ANG));
    }
    //If there are no error flags
    else if (!state.flags.undervoltage && !state.flags.pm_overdrive && !state.flags.sw_down && !state.flags.sw_up)
    {
      stepper->moveTo(set_pos); // Move motor to set position
      Serial.println("gowl=" + String((float)set_pos * POS_TO_ANG));
    }
    else if (state.flags.sw_down && set_pos > stepper->getCurrentPosition()) //min limit is reached but new position is higher
    {
      stepper->moveTo(set_pos); // Move motor to set position
      Serial.println("gowl=" + String((float)set_pos * POS_TO_ANG));
    }
    else if (state.flags.sw_up && set_pos < stepper->getCurrentPosition()) // max limit is reached but new position is lower
    {
      stepper->moveTo(set_pos); // Move motor to set position
      Serial.println("gowl=" + String((float)set_pos * POS_TO_ANG));
    }
    else
    {
      Serial.println("ERROR: "+get_flag_string(&state));  //Send error message
    }
  }
  else if (inputString.equalsIgnoreCase("savewl"))
  {
    save_pos = stepper->getCurrentPosition(); // get the current position to save
    EEPROM_Data.begin("my-app", false);
    EEPROM_Data.putLong("save_pos", save_pos);
    EEPROM_Data.end(); // Close the Preferences
    Serial.println("savewl=" + String((float)save_pos * POS_TO_ANG));
  }
  else if (inputString.equalsIgnoreCase("getwl"))
  {
    long pos = stepper->getCurrentPosition();
    Serial.println("getwl=" + String((float)pos * POS_TO_ANG)+"=G");  //
  }
  else if (inputString.equalsIgnoreCase("getspeed"))
  {
    long pos = stepper->getCurrentSpeedInMilliHz();
    Serial.println("getspeed=" + String(pos));
  }
  else if (inputString.equalsIgnoreCase("get_mot_state"))
  {
    if (stepper->isRunning())
    {
      Serial.println("get_mot_state=run");
    }
    else
    {
      Serial.println("get_mot_state=stop");
    }
  }
  else if (inputString.equalsIgnoreCase("getvolt"))
  {
    Serial.println("getvolt=" + String(get_signal(),5)); // print voltage
  }
  else if (inputString.equalsIgnoreCase("getpmt"))
  {
    Serial.println("getpmt=" + String(pmt.curr)); // print Photomultiplier control voltage 
  }
  else if (inputString.equalsIgnoreCase("getpmt_min"))
  {
    Serial.println("getpmt_min="+ String(pmt.min)); // print Photomultiplier control MIN_voltage 
  }
  else if (inputString.equalsIgnoreCase("getpmt_max"))
  {
    Serial.println("getpmt_max=" + String(pmt.max)); // print Photomultiplier control MAX_voltage 
  }
  else if (inputString.startsWith("setpmt="))
  {
    int set_pmt = inputString.substring(7).toInt();
    if (set_pmt < pmt.min){
      Serial.println("ERROR: PMT MIN value is:" + String(pmt.min));
    }
    else if (set_pmt > pmt.max){
      Serial.println("ERROR: PMT MAX value is:" + String(pmt.max));
    }
    else
    {
      pmt.curr = set_pmt;
      pmt.curr_mvolt = map(pmt.curr, pmt.min, pmt.max, pmt.min_mvolt, pmt.max_mvolt);
      pmt.dac_value = map(pmt.curr_mvolt, pmt.min_mvolt, pmt.max_mvolt, pmt.min_dac_value, pmt.max_dac_value);
      //pmt.dac_value = map(pmt.curr, pmt.min, pmt.max, 0, 255);  //Map the percentage value to 0-255
      dacWrite(DAC_PM_PIN, pmt.dac_value);
      Serial.println("setpmt=" + String(set_pmt));
    }
  }
  else if (inputString.startsWith("setpmt_offset="))
  {
    int index = inputString.indexOf('=');
    int set_pmt_offset = inputString.substring(index+1).toInt();
    if (set_pmt_offset < pmt.dac_offset_min){
      Serial.println("ERROR: PMT OFFSET MIN value is:" + String(pmt.dac_offset_min));
    }
    else if (set_pmt_offset > pmt.dac_offset_max){
      Serial.println("ERROR: PMT OFFSET MAX value is:" + String(pmt.dac_offset_max));
    }
    else
    {
      pmt.dac_offset = set_pmt_offset;
      //pmt.dac_value = map(pmt.curr, pmt.min, pmt.max, 0, 255);  //Map the percentage value to 0-255
      //dacWrite(DAC_OFFSET_PIN, pmt.dac_offset);
      Serial.println("setpmt_offset=" + String(pmt.dac_offset));
    }
  }
  else if (inputString.equalsIgnoreCase("get_amp_mode"))
  {
    String amp_mode = "";
    if (state.flags.gain_auto_mode) //AUTO MODE
    {
      amp_mode = "AUTO=;VAL=";
    }
    else{
      amp_mode = "MAN=;VAL="; // Manual Mode
    }
    if (!state.flags.gain_ON) //if OPAMP Gain is disabled (1x)
    {
      Serial.println("get_amp_mode=" + amp_mode + "1x=G");
    }
    else if (state.flags.gain_ON)
    {
      switch (i2c_adc.getGain())
      {
      case GAIN_TWO:
        Serial.println("get_amp_mode=" + amp_mode + "16x=G");
        break;
      case GAIN_FOUR:
        Serial.println("get_amp_mode=" + amp_mode + "32x=G");
        break;
      case GAIN_EIGHT:
        Serial.println("get_amp_mode=" + amp_mode + "64x=G");
        break;
      default:
        break;
      }
    }
  }
  else if (inputString.startsWith("set_amp_mode="))
  {
    if (inputString.equalsIgnoreCase("set_amp_mode=AUTO"))
    {
      state.flags.gain_auto_mode = true;
      Serial.println("set_amp_mode=AUTO");
    }
    else if (inputString.equalsIgnoreCase("set_amp_mode=16x"))
    {
      state.flags.gain_auto_mode = false;
      state.flags.gain_ON = true; //Enable 16x OPAMP Gain
      digitalWrite(GAIN_OUT, state.flags.gain_ON);
      i2c_adc.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
      i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
      if (state.debug){Serial.println("Set GAIN_TWO +/- 2.048V");}
      Serial.println("set_amp_mode=16x");
    }
    else if (inputString.equalsIgnoreCase("set_amp_mode=1x"))
    {
      state.flags.gain_auto_mode = false;
      state.flags.gain_ON = false; //Disable 16x OPAMP Gain
      digitalWrite(GAIN_OUT, state.flags.gain_ON);
      i2c_adc.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
      i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
      if (state.debug){Serial.println("Set GAIN_TWO +/- 2.048V");}
      Serial.println("set_amp_mode=1x");
    }
    else if (inputString.equalsIgnoreCase("set_amp_mode=32x"))
    {
      state.flags.gain_auto_mode = false;
      state.flags.gain_ON = true; //Enable 16x OPAMP Gain
      digitalWrite(GAIN_OUT, state.flags.gain_ON);
      i2c_adc.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
      i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
      if (state.debug){Serial.println("Set GAIN_FOUR +/- 1.024V");}
      Serial.println("set_amp_mode=32x");
    }
    else if (inputString.equalsIgnoreCase("set_amp_mode=64x"))
    {
      state.flags.gain_auto_mode = false;
      state.flags.gain_ON = true; //Enable 16x OPAMP Gain
      digitalWrite(GAIN_OUT, state.flags.gain_ON);
      i2c_adc.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
      i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
      if (state.debug){Serial.println("Set GAIN_EIGHT +/- 0.512V");}
      Serial.println("set_amp_mode=64x");
    }
    else{
      Serial.println("ERROR: Amplifier Mode Invalid");
    }
  }
  else if (inputString.startsWith("set_filter_mode="))
  {
    if (inputString.equalsIgnoreCase("set_filter_mode=OFF"))
    {
      digitalWrite(TK1_OUT, 0); //Disable filter SLOW
      digitalWrite(TK2_OUT, 0); //Disable filter FAST
      Serial.println("set_filter_mode=OFF=;fc==G");
    }
    else if (inputString.equalsIgnoreCase("set_filter_mode=SLOW"))
    {
      digitalWrite(TK2_OUT, 0); //Disable filter FAST
      digitalWrite(TK1_OUT, 1); //Enable filter SLOW
      Serial.println("set_filter_mode=SLOW=;fc=" + String(TK1_FC) + "=G");
    }
    else if (inputString.equalsIgnoreCase("set_filter_mode=FAST"))
    {
      digitalWrite(TK1_OUT, 0); //Disable filter SLOW
      digitalWrite(TK2_OUT, 1); //Enable filter FAST
      Serial.println("set_filter_mode=FAST=;fc=" + String(TK2_FC) + "=G");
    }
    else{
      Serial.println("ERROR: Filter Mode Invalid");
    }
  }
  else if (inputString.startsWith("scan="))
  {
    int index = inputString.indexOf('=');
    float start_pos_f = inputString.substring(index+1).toFloat();
    long start_pos = round(start_pos_f * ANG_TO_STEP);
    index = inputString.indexOf(';',index+1);
    float stop_pos_f = inputString.substring(index+1).toFloat();
    long stop_pos = round(stop_pos_f * ANG_TO_STEP);
    index = inputString.indexOf(';',index+1);
    float speed_f = inputString.substring(index+1).toFloat();  //speed is Å/sec
    int speed = speed_f * ANG_TO_STEP; //speed in steps/sec
    if (stop_pos <= start_pos)
    {
      Serial.println("ERROR: Stop Wavelength must be higher than Start Wavelength");
    }
    else if (start_pos < POS_MIN || start_pos > POS_MAX)
    {
      Serial.println("ERROR: Start Wavelength MIN is:" + String((float)POS_MIN * POS_TO_ANG) + " and Max is:" + String((float)POS_MAX * POS_TO_ANG));
    }
    else if (stop_pos < POS_MIN || stop_pos > POS_MAX)
    {
      Serial.println("ERROR: Stop Wavelength MIN is:" + String((float)POS_MIN * POS_TO_ANG) + " and Max is:" + String((float)POS_MAX * POS_TO_ANG));
    }
    else if (speed< MIN_SPEED || speed > MAX_SPEED)
    {
      Serial.println("ERROR: Wrong Speed, MIN is:" + String((float)MIN_SPEED * POS_TO_ANG) + " and Max is:" + String((float)MAX_SPEED * POS_TO_ANG));
    }
    //If there are error Flags
    else if (state.flags.undervoltage || state.flags.pm_overdrive || state.flags.sw_down || state.flags.sw_up)
    {
      Serial.println("ERROR: "+get_flag_string(&state));  //Send error message
    }
    else
    {
      run_scan.start_wl = start_pos;
      run_scan.stop_wl = stop_pos;
      run_scan.speed = speed;
      xTaskCreatePinnedToCore(scan_task, "scan_task", 2048, NULL, 8, NULL, 1); // task to core
    }
  }
  else if (inputString.startsWith("getmsm_dur="))  //example getmsm_dur=2000;2500;70 ->start;stop;resolution,speed
  {
    int index = inputString.indexOf('=');
    float start_pos_f = inputString.substring(index+1).toFloat();
    long start_pos = round(start_pos_f * ANG_TO_STEP);
    index = inputString.indexOf(';',index+1);
    float stop_pos_f = inputString.substring(index+1).toFloat();
    long stop_pos = round(stop_pos_f * ANG_TO_STEP);
    index = inputString.indexOf(';',index+1);
    float speed_f = inputString.substring(index+1).toFloat();
    int speed = speed_f * ANG_TO_STEP;
    if (start_pos >= POS_MIN && stop_pos > start_pos && stop_pos <= POS_MAX && speed >= MIN_SPEED && speed <= MAX_SPEED )
    {
      long meas_dur=0;
      long pos = stepper->getCurrentPosition();
      if((stop_pos-start_pos)<sq(speed)/ACCEL) //if the number of steps is not enough to reach MAX_SPEED
      {
        int new_vel_max = sqrt((stop_pos-start_pos)*ACCEL);  //calculate the max speed for this movement
        meas_dur = (2*new_vel_max)/ACCEL;
      }
      else
      {
        int time_in_const = (int)((abs(stop_pos-start_pos)-(ACCEL*sq(speed/ACCEL)))/speed);
        meas_dur = (2*(speed/ACCEL))+time_in_const;
      }
      Serial.println("getmsm_dur=" + String(meas_dur));
    }
    else
    {
      Serial.println("ERROR: getmsm_dur: Invalid input parameters");
    }
  }
  else if (inputString.startsWith("get_res="))  //get resolution, example: get_res=70
  {
    int index = inputString.indexOf('=');
    float speed_f =  inputString.substring(index+1).toFloat();
    int speed = speed_f * ANG_TO_STEP;
    if ( !(speed >= MIN_SPEED && speed <= MAX_SPEED))
    {
      Serial.println("ERROR: Speed MIN is:" + String((float)MIN_SPEED * POS_TO_ANG) + " and Max is:" + String((float)MAX_SPEED * POS_TO_ANG));
    }
    else
    {
      Serial.println("get_res=" + String((float)(get_resolution(speed)) * POS_TO_ANG));
    }
  }
  else if (inputString.equalsIgnoreCase("getspeed_min"))
  {
    Serial.println("getspeed_min=" + String((float)MIN_SPEED * POS_TO_ANG)); // print min speed 
  }
  else if (inputString.equalsIgnoreCase("getspeed_max"))
  {
    Serial.println("getspeed_max=" + String((float)MAX_SPEED * POS_TO_ANG)); // printmax speed 
  }
  else if (inputString.equalsIgnoreCase("getscan"))
  {
    if (run_scan.moving_to_start)
    {
      Serial.println("scan=moving_to_start");
    }
    else if (run_scan.scanning)
    {
      Serial.println("scan=scanning");
    }
    else if (run_scan.reads_done > 0)
    {
      Serial.println("scan=reads_done="+String(run_scan.reads_done));
    }
    else
    {
      Serial.println("scan=not_active");
    }
  }
  else if (inputString.startsWith("debug="))
  {
    state.debug = inputString.substring(6).toInt();
    Serial.println("debug=" + String(state.debug));
  }
  else if (inputString.equalsIgnoreCase("stop"))
  {
    run_scan.stop = true;
    stepper->stopMove();
    if (stepper->isRunning())
    {
      Serial.println("stop=stopping");
    }
    else
    {
      Serial.println("stop=done");
    }
  }
  else if (inputString.equalsIgnoreCase("reboot"))
  {
    ESP.restart();
  }
  else
  {
    Serial.println("New_command:" + inputString); // print the command
    Serial.println(F(" FAIL >> Type: help"));
  }
}

/* ## Verify if there is a new Serial command ## */
bool Is_New_Command(void)
{
  while (Serial.available() > 0)
  {
    inputString = Serial.readStringUntil('\r');
    while (Serial.available() > 0)
    {
      Serial.read(); // reads to the end of the buffer usually just "\n"
    }
    // inputString = Serial.readString();
    return true;
  }
  return false;
}

void CMD_reader(void *pvParameters) {
//void CMD_reader(void)
//{
  while (1) {
    if (Is_New_Command())
    {
      Process_Command();
    }
  // vTaskDelay(200);
  delay(20); // ms to wait so that main tasks run
  }
  vTaskDelete(NULL);
}

void task_blink_led(void *parameter)
{
  while (1)
  {
  state.led_stat_on = !state.led_stat_on;
  digitalWrite(LED_STAT_OUT, state.led_stat_on); //LED is changing state
  delay(state.led_blink_time); // wait
  }
  vTaskDelete(NULL);
}

void Read_EEPROM_Data(void)
{
  EEPROM_Data.begin("my-app", true);                          // True for Read-only mode
  unsigned int boot_count = EEPROM_Data.getUInt("counter", 0); // Get the counter value, if the key does not exist, return 0
  boot_count++;                                                // increase BOOT count
  EEPROM_Data.putUInt("counter", boot_count);                  // Write Boot count
  Serial.println("Current boot counter=" + String(boot_count));
  ssid = EEPROM_Data.getString("ssid", ssid);
  Serial.println("SSID=" + String(ssid));
  password = EEPROM_Data.getString("password", password);
  Serial.println("Passwd len=" + String(password.length()));
  // Serial.println("Passwd=" + String(password));
  save_pos = EEPROM_Data.getLong("save_pos", save_pos);
  stepper->setCurrentPosition(save_pos); // Set the memorized position into the driver
  Serial.println("save_pos=" + String(save_pos) + " : Wavelength(Å)=" + String((float)save_pos * POS_TO_ANG));
  state.wifistart = EEPROM_Data.getBool("wifistart" , state.wifistart);
  Serial.println("Wifistart=" + String(state.wifistart));
  EEPROM_Data.end(); // Close the Preferences
}

void start_WIFI()
{
  WiFi.begin(ssid.c_str(), password.c_str()); // try to connect
  Serial.print("Establishing connection to WiFi.");
  int timeout = 6;
  while (true)
  {
    delay(1000);
    if (WiFi.status() == WL_CONNECTED)
    {
      Serial.println("");
      Serial.print("WiFi Ready! Go to: http://");
      Serial.println(WiFi.localIP());
      break;
    }
    Serial.print(".");
    timeout--;
    if (timeout <= 1) // timout: it could not connect to the given ssid
    {
      Serial.println("");
      WiFi.disconnect(false);
      Serial.println("Timout: it could not connect to SSID:" + String(ssid));
      Serial.println("Start Access Point ");
      ssid = def_ssid;
      password = def_password;
      WiFi.softAP(ssid.c_str(), password.c_str()); // try to connect
      int timeout = 4;
      while (timeout >= 1)
      {
        timeout--;
        if (WiFi.softAP(ssid.c_str(), password.c_str()))
        {
          IPAddress myIP = WiFi.softAPIP();
          Serial.print("WiFi Ready! Go to: http://");
          Serial.println(myIP);
          break;
        }
        else
        {
          Serial.println("Problem Creating Wifi Network");
        }
      }
      break;
    }
  }

  if (!MDNS.begin(hostname)) // http://<hostname>.local}
  {
    Serial.println("ERROR: setting up MDNS");
  }
  else
  {
    Serial.println("MDNS started, use:" + String(hostname) + ".local");
  }

  QueueHandle = xQueueCreate(QueueElementSize, sizeof(doc_queue));
    // Check if the queue was successfully created
  if(QueueHandle == NULL){
    Serial.println("ERROR: Queue could not be created. Halt.");
  }

  initWebSocket();

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            {
    //request->send_P(200, "text/html", index_html, processor);
    request->send(200, "text/html", index_html); });
  // Start server
  server.begin();
  Serial.println("HTTP server started");
    ///* Function, name of the task,stack size,input_param, prority, task handle, Core
  xTaskCreatePinnedToCore(loopWebClients, "loopWebClients", 2048, NULL, 5, NULL, 1); // task to core
  Serial.println("loopWebClients task started");
}

void setup()
{
  Serial.begin(115200);
  Serial.setTimeout(5000); // serial will timout in 5seconds
  Serial.setDebugOutput(false);

  print_version();

  inputString.reserve(200);                                   // reserve 200 bytes for the inputString
  Serial.println("Free Memory=" + String(ESP.getFreeHeap())); // Free Memory=306832 bytes 300KBytes

  pinMode(PM_RESET_OUT, OUTPUT);
  digitalWrite(PM_RESET_OUT,0); //D Type Flip-Flop
  //pinMode(DAC_OFFSET_PIN, INPUT_PULLDOWN); //Do not use offset DAC
  pinMode(DAC_OFFSET_PIN, OUTPUT); //Do not use offset DAC
  //dacWrite(DAC_OFFSET_PIN, pmt.dac_offset);
  digitalWrite(DAC_OFFSET_PIN,0);
  pinMode(LED_STAT_OUT, OUTPUT);
  //digitalWrite(LED_STAT_OUT, state.led_stat_on);
  pinMode(GAIN_OUT, OUTPUT);
  digitalWrite(GAIN_OUT, 0); //disable 16x OPAMP Gain
  pinMode(TK1_OUT, OUTPUT);
  digitalWrite(TK1_OUT, 0); //disable filter SLOW
  pinMode(TK2_OUT, OUTPUT);
  digitalWrite(TK2_OUT, 0); //disable filter FAST
  digitalWrite(QUARTER_STEP_OUT, 0); //Use 1/2 step
  pinMode(QUARTER_STEP_OUT, OUTPUT);
  digitalWrite(QUARTER_STEP_OUT, 0); //Use 1/2 step
  //digitalWrite(QUARTER_STEP_OUT, 1); //Use 1/4 step

  pinMode(PM_OVERDRIVE_IN, INPUT);
  state.flags.pm_overdrive = digitalRead(PM_OVERDRIVE_IN);  //get the status of the PIN
  attachInterrupt(PM_OVERDRIVE_IN, isr_pm_overdrive, RISING);

  pinMode(SW_DOWN_IN, INPUT_PULLUP);
  state.flags.sw_down = !digitalRead(SW_DOWN_IN); //get the status of the PIN
  attachInterrupt(SW_DOWN_IN, isr_sw_down, FALLING);

  pinMode(SW_UP_IN, INPUT_PULLUP);
  state.flags.sw_up = !digitalRead(SW_UP_IN);  //get the status of the PIN
  attachInterrupt(SW_UP_IN, isr_sw_up, FALLING);
  
  pinMode(UNDERVOLTAGE_PIN, INPUT);
  state.flags.undervoltage = digitalRead(UNDERVOLTAGE_PIN);
  if (state.flags.undervoltage){
    Serial.println(state.flags.undervoltage_name);
  }
  else{
    Serial.println("INPUT Voltage is normal");
  }
  attachInterrupt(UNDERVOLTAGE_PIN, isr_undervoltage, RISING);

  ///* Function, name of the task,stack size,input_param, prority, task handle, Core
  xTaskCreatePinnedToCore(task_blink_led, "task_blink_led", 2048, NULL, 2, NULL, 1); // task to core

  // Stepper configuration with Step Direction and Enable pins. //A4988 Driver
  engine.init();
  stepper = engine.stepperConnectToPin(MOT_STEP_OUT);
  if (stepper)
  {
    stepper->setDirectionPin(MOT_DIR_OUT);
    stepper->setEnablePin(MOT_EN_OUT, true); // Low active
    stepper->setAutoEnable(true);
    //stepper->setSpeedInUs(MAX_SPEED); // the parameter is us/step !!!
    stepper->setSpeedInHz(MAX_SPEED); // the parameter is steps/sec
    stepper->setAcceleration(ACCEL);
  }
  else
  {
    Serial.println("Stepper Not initialized!");
  }

  Read_EEPROM_Data(); // Read stored data parameters in the memory

  if (!i2c_adc.begin())
  {
    Serial.println("Failed to initialize I2C ADC.");
    Serial.println("Using the Internal ADC on PIN:" + String(ADC0_IN));
    analogSetPinAttenuation(ADC0_IN, ADC_11db);
    adcAttachPin(ADC0_IN);
    use_I2C_ADC = false;
  }
  else
  {
    // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
    //i2c_adc.setGain(GAIN_ONE); // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
    i2c_adc.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
    // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
    // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
    // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
    //i2c_adc.setDataRate(RATE_ADS1115_128SPS); ///< 128 samples per second (default)
    //i2c_adc.setDataRate(RATE_ADS1115_250SPS); ///< 250 samples per second
    i2c_adc.setDataRate(RATE_ADS1115_860SPS); ///< 860 samples per second
    // Start ADC continuous mode 
    i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, /*continuous=*/true);
    // i2c_adc.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_0_1, /*continuous=*/false);
    //                                                                ADS1015  ADS1115

    pinMode(ADC_RDY_IN, INPUT);
    // We get a falling edge every time a new sample is ready. :TODO: maybe there is a conflict with the debugger interface?
    attachInterrupt(ADC_RDY_IN, isr_ADC_data_ready, FALLING);
    //attachInterrupt(digitalPinToInterrupt(ADC_RDY_IN), isr_ADC_data_ready, FALLING);
    Serial.println("I2C ADC Initialized");
    use_I2C_ADC = true;
  }

  if(state.wifistart)
  {
    start_WIFI();
  }
  else{
    Serial.println("Wifi NOT ENABLED");
  }

  pmt.curr_mvolt = map(pmt.curr, pmt.min, pmt.max, pmt.min_mvolt, pmt.max_mvolt);
  pmt.dac_value = map(pmt.curr_mvolt, pmt.min_mvolt, pmt.max_mvolt, pmt.min_dac_value, pmt.max_dac_value);
  //pmt.dac_value = map(pmt.curr, pmt.min, pmt.max, 0, 255);  //Map the percentage value to 0-255
  dacWrite(DAC_PM_PIN, pmt.dac_value);
  Serial.println("PMT Voltage=" + String(pmt.curr));

  xTaskCreatePinnedToCore(CMD_reader, "CMD_reader", 2048, NULL, 4, NULL, 1); // task to core
  state.led_blink_time = BLINK_SLOW;
  Serial.println("SETUP DONE");
}// END of setup()

void loop()
{
  if(state.wifistart){ ws.cleanupClients(); }
  state_handler(); // inside is also handled undervoltage event(use in a loop with less than 30ms delay)
  delay(20); // wait so that other tasks run
}
