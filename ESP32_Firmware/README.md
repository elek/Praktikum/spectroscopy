## How to direct Flash Binary

### Esptool For linux
Using the the correct files and correct addresses as below:
```
$sudo apt-get install esptool
```
```
$esptool --chip esp32 --port "/dev/ttyUSB0" --baud 460800 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size 4MB 0x1000 /home/student/spectroscopy/ESP32_Firmware/bin/bootloader.bin 0x8000 /home/student/spectroscopy/ESP32_Firmware/bin/partitions.bin 0xe000 /home/student/spectroscopy/ESP32_Firmware/bin/boot_app0.bin 0x10000 /home/student/spectroscopy/ESP32_Firmware/bin/firmware.bin
```
https://docs.espressif.com/projects/esptool/en/latest/esp32/

### For Windows
Download the Flash Download Tools  
http://espressif.com/en/support/download/other-tools  
![Esptool](Pics/esptool.png)

## For Developers
### Installing For PlatformIO
- https://code.visualstudio.com/
- In "extensions" add "platformio ide"
- sudo apt-get install libpython2.7 libpython2.7-dev
- sudo apt-get install python3-venv
- sudo apt-get install curl
- curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core/develop/platformio/assets/system/99-platformio-udev.rules | sudo tee /etc/udev/rules.d/99-platformio-udev.rules
- sudo service udev restart
- sudo usermod -a -G dialout $USER
- sudo usermod -a -G plugdev $USER

### Installing For Arduino
- Arduino IDE 2.2.0
- esp32 board (Version 2.0.3) 

Open the Arduino Software.  
Edit Preferences: "File"->"Preferences"->  
Add the ESP32 resources in the "Aditional Boards Manager URL's:"  
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json  
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json  

- Change the Project location in the arduino application in "Sketchbook location:"  
Should look like: /home/student/spectroscopy/ESP32_Firmware

## Schematics, Datasheets
![Microcontroller, acquisition board](Pics/schematic.pdf)

![Photomultiplier R928](Pics/R928_R928P_R955_R955P_TPMS1091E.pdf)

![Photomultiplier controller board](Pics/Sideon_Photomultiplier_Preamp.pdf)

![Photomultiplier socket datasheet](Pics/SOCKET_HAMAMATSU_C956-04.pdf)
