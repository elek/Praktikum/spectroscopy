# from examplegraph import Ui_Dialog
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThread, QObject, pyqtSignal
from PyQt5.QtWidgets import QMessageBox, QWidget, QPushButton, QApplication
from pyqtgraph import PlotWidget
from pyqtgraph import exporters
import pyqtgraph as pg

from serialCMD import SerialLib
import pathlib
import numpy as np
import random
import sys
import time
import tkinter
import tkinter.filedialog
import os
from copy import copy
from decimal import Decimal
import concurrent.futures
from io import StringIO
import re
import datetime
import threading
from gui_design import Ui_Dialog


class loop_thread(QObject):
    do_new_loop = pyqtSignal()

    def run(self):
        while True:
            if QThread.currentThread().isInterruptionRequested():
                print("Requested to stop Thread")
                return
            self.do_new_loop.emit()
            time.sleep(0.5)  # seconds

class Scan:
    meas_speed = 50.0  # startup value for the measurement speed
    start_wl = 2000.0  # startup Start value for measurement
    stop_wl = 8000.0  # startup Stop value for measurement
    resolution = 0.1
    pmt_val = 100  # holds the value of Photomultiplier control voltage
    wavelength_unit = "Å"  # configure the unit (nm or Å)
    filter_mode = "OFF"
    filter_cutoff = ""  #cutoff Frequency It will get the value from the hardware
    amp_gain = "1x"

# initialize GUI
class Ui_Dialog_new(QtWidgets.QDialog, Ui_Dialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.ser_lib = SerialLib(self)
        self.data_curve = None #DATA set to be displayed
        self.vis_spectrum = None #Visible light spectrum bar
        self.inf1 = None #infinite line
        self.inf2 = None  #infinite line
        self.start_butt.clicked.connect(self.startmsm)
        self.start_butt.setDefault(False)  #Avoid starting measurement when pressing ENTER key
        self.stop_butt.clicked.connect(self.stopbutton)
        self.stop_butt.setDefault(False)
        self.save_butt.clicked.connect(self.save)
        self.wl_unit_box.currentIndexChanged.connect(self.unit_changed)
        self.cursors_butt.clicked.connect(self.find_lines_min_max)
        self.gotowl_butt.clicked.connect(self.move_to_wl)
        self.new_wl_butt.clicked.connect(self.setnewwl)
        self.sig_amp_box.currentIndexChanged.connect(self.amp_changed)
        self.filter_box.currentIndexChanged.connect(self.filter_changed)
        self.startwl_val.textEdited.connect(self.params_changed)
        self.stopwl_val.textEdited.connect(self.params_changed)
        self.meas_speed_val.textEdited.connect(self.meas_speed_val_changed)
        self.pmt_box_val.valueChanged.connect(self.pmt_box_val_changed)
        self.pmt_slider_val.valueChanged.connect(self.pmt_slider_val_changed)
        self.meas_speed_slider_val.valueChanged.connect(self.meas_speed_slider_val_changed)
        self.currwl_val.setReadOnly(True)
        self.meas_dur_val.setReadOnly(True)
        self.meas_res_val.setReadOnly(True)
        self.cursors_butt.setEnabled(False)
        #self.wl_unit_box.hide() #TODO
        #self.wl_unit_text.hide() #TODO
        self.new_wl_butt.setText("Calibrate Wavelength")
        self.inf1 = pg.InfiniteLine(movable=True, angle=90, label='λ={value:0.2f}Å',
                                    labelOpts={'position': 0.9, 'color': (200, 200, 100),
                                               'fill': (200, 200, 200, 50), 'movable': True})
        self.inf2 = pg.InfiniteLine(movable=True, angle=90, label='λ={value:0.2f}Å',
                                    labelOpts={'position': 0.9, 'color': (200, 200, 100),
                                               'fill': (200, 200, 200, 50), 'movable': True})
        self.inf1.sigPositionChanged.connect(self.line1_moved)
        self.inf2.sigPositionChanged.connect(self.line2_moved)
        self.curvolt = 0.0  # Get ADC voltage
        self.curwl = 0.0  # holds the current_wl to display
        self.factor_mv = 500
        self.inmsmloop = False
        self.saved = True
        self.stop_msm = False
        self.timeout = 0
        self.scan = Scan()
        self.saveScan = None  # It will save the parameters for the scan
        self.meas_status = 0  # 0-measurement not running; 1-measurement start; 2-measurement-running
        self.meas_duration = 0 # claculated measurement time
        self.meas_start_time = 0.0 #saves the time at start of a measurement
        self.datasize = 15000*100 #The maximum datasize is 15000Å * 100(maximal_resolution)
        self.plot_wl = [0]*self.datasize  #List that stores the wavelengths data of a measurement
        self.plot_volt = [0]*self.datasize #List that store the voltages data of a measurement
        self.n_samples = 0 #Number of Samples scanned
        self.meas_stop_signal = False  #Flag used to tell when a measurement is finished
        self.set_init_graph_settings()
        self.thread_display = QThread()
        self.loop = loop_thread()
        self.loop.moveToThread(self.thread_display)
        self.loop.do_new_loop.connect(self.display_show)  # when new data is available update display
        self.thread_display.started.connect(self.loop.run)
        self.wl_unit_box.setCurrentIndex(0) # unit should start with Å ... 0-Å, 1-nm
        self.wl_unit_box.removeItem(1)  #TODO disabled unit "nm" for now
        self.unit_changed()

        if self.varsetup() == True:  # If starup is sucessfull
            self.thread_display.start()
        else:
            self.lock_buttons()


    def reject(self):  #if ESC key is pressed the reject(self) function is called and the event close() is triggered
        self.close()

    def lock_buttons(self):
        # disable buttons except stop
        self.pmt_slider_val.setEnabled(False)
        self.meas_speed_slider_val.setEnabled(False)
        self.stop_butt.setEnabled(True)
        self.start_butt.setEnabled(False)
        self.save_butt.setEnabled(False)
        self.gotowl_butt.setEnabled(False)
        self.new_wl_butt.setEnabled(False)
        self.filter_box.setEnabled(False)
        self.sig_amp_box.setEnabled(False)

        # set fields to read-only
        self.startwl_val.setReadOnly(True)
        self.stopwl_val.setReadOnly(True)
        self.meas_speed_val.setReadOnly(True)
        # self.meas_res_val.setReadOnly(True)
        self.gowl_val.setReadOnly(True)
        self.pmt_box_val.setReadOnly(True)

    def unlock_buttons(self):
        # enable buttons except stop
        self.pmt_slider_val.setEnabled(True)
        self.meas_speed_slider_val.setEnabled(True)
        self.stop_butt.setEnabled(False)
        self.start_butt.setEnabled(True)
        if not self.saved:
            self.save_butt.setEnabled(True)
        self.gotowl_butt.setEnabled(True)
        self.new_wl_butt.setEnabled(True)
        self.filter_box.setEnabled(True)
        self.sig_amp_box.setEnabled(True)

        # set fields to read-only
        self.startwl_val.setReadOnly(False)
        self.stopwl_val.setReadOnly(False)
        self.meas_speed_val.setReadOnly(False)
        # self.meas_res_val.setReadOnly(False)
        self.gowl_val.setReadOnly(False)
        self.pmt_box_val.setReadOnly(False)

    def set_graph_settings(self):
        self.data_curve.setData([],[],clear=True)
        #self.graphicsView.plot(clear=True)
        #self.graphicsView.setLimits(xMin=self.start_wl, xMax=8000 + 5 * 0.1, minXRange=10, yMin=-0.01, yMax=10, minYRange=0.01)
        self.graphicsView.setLimits(xMin=self.scan.start_wl, xMax=self.scan.stop_wl + 5 * self.scan.resolution)
        #self.graphicsView.setRange(xRange=[2000.0, 15000.0], yRange=[0, 10])
        self.graphicsView.setRange(xRange=[self.scan.start_wl, self.scan.stop_wl],yRange=[0, 10])
        self.graphicsView.enableAutoRange(x=False, y=True)
        self.graphicsView.setAutoVisible(x=False, y=True)

    def set_init_graph_settings(self):
        #self.graphicsView.setLabel('left', "Voltage" , units='V')
        self.graphicsView.setLabel('left', "Voltage (V)")
        self.graphicsView.setLabel('bottom', "Wavelength (Å)")
        self.graphicsView.setMouseEnabled(x=True, y=False)
        #self.graphicsView.setMouseEnabled(x=True, y=True)
        self.graphicsView.showGrid(x=True, y=True, alpha=0.5)
        self.graphicsView.setLimits(xMin=2000.0, xMax=15000 + 5 * 0.1, minXRange=0.1, yMin=-0.2, yMax=10, minYRange=0.001)
        # self.graphicsView.setLimits(xMin=self.start_wl, xMax=self.stop_wl + 5 * self.resolution, minXRange=10, yMin=-0.01, yMax=10, minYRange=0.01)
        self.graphicsView.setRange(xRange=[2000.0, 15000.0],yRange=[0, 10])
        self.graphicsView.enableAutoRange(x=False, y=True)
        self.graphicsView.setAutoVisible(x=False, y=True)
        # self.graphicsView.setRange(xRange=[self.start_wl, self.stop_wl], padding=1)
        # self.graphicsView.setDefaultPadding(1)
        vis_lim = (380, 750)
        wl = np.arange(vis_lim[0], vis_lim[1] + 1, 1) #make an array for wl
        wl_norm = (wl - np.min(wl)) / (np.max(wl) - np.min(wl))  #normalize all values to be between 0 and 1
        colorlist = list(zip(wl_norm, [self.wavelength_to_rgb(w) for w in wl]))
        pos, color = zip(*colorlist)
        cm_vis= pg.ColorMap(pos, color)
        pen_vis = cm_vis.getPen( span=(3800.0,7500.0), width=50, orientation='horizontal')
        #self.vis_spectrum = pg.PlotCurveItem(pen=pen_vis)
        self.vis_spectrum = pg.PlotDataItem(pen=pen_vis)
        self.graphicsView.addItem(self.vis_spectrum)
        #self.vis_spectrum.setData([3800,7500],[0,0])

        #pg.ColorMap(name="UV"))
        cm_uv = pg.colormap.get('CET-L8')
        pen_uv = cm_uv.getPen(span=(2000, 1), width=50, orientation='horizontal')
        # self.uv_spectrum = pg.PlotCurveItem(pen=pen_uv)
        #self.uv_spectrum = pg.TextItem("UV")
        # self.graphicsView.addItem(self.uv_spectrum)

        #cm_ir = pg.ColorMap(name="IR")
        cm_ir = pg.colormap.get('CET-L8')
        pen_ir = cm_ir.getPen(span=(7500.0, 15000.0), width=50, orientation='horizontal')
        #self.ir_spectrum = pg.PlotCurveItem(pen=pen_ir)
        #self.graphicsView.addItem(self.ir_spectrum)
        #self.data_curve = pg.PlotCurveItem()
        self.data_curve = pg.PlotDataItem() #DATA TO BE DISPLAYED
        self.graphicsView.addItem(self.data_curve)
        self.graphicsView.sigYRangeChanged.connect(self.updateY)
        self.graphicsView.sigXRangeChanged.connect(self.updateY)
        self.updateY()

    def updateY(self):  #Update visible spectrum bar so that is always on the bottom of the graph
        #print("UPDATED VIEW")
        self.vis_spectrum.clear()  #clear this bar so that autorange works just for the data
        self.graphicsView.enableAutoRange(x=False, y=True)
        self.graphicsView.setAutoVisible(x=False, y=True)
        # try:
            #print(self.data_curve.dataRect().getCoords()) #Check the m bounds of the data
        x_low, y_low, x_high, y_high = self.graphicsView.viewRect().getCoords()
        #print("graphicsView=" + str(y_low))
        self.graphicsView.enableAutoRange(x=False, y=False)
        self.vis_spectrum.setData([3800.0, 7500.0], [y_high, y_high])

        #self.uv_spectrum.setData([2000.0, 3800.0], [y_low, y_low])
        #self.ir_spectrum.setData([7500.0, 15000.0], [y_low, y_low])


    def find_lines_min_max(self):
        if self.n_samples > 0:
            self.inf1.setBounds([self.scan.start_wl, self.curwl])
            self.inf2.setBounds([self.scan.start_wl, self.curwl])
            # self.inf1.setPos(self.start_wl + (self.curwl - self.start_wl) / 3)
            array = np.asarray(self.plot_volt[0:self.n_samples])
            index1 = (np.abs(array - max(self.plot_volt[0:self.n_samples])).argmin())  # Find the index where the plot_volt value is the maximum
            index2 = (np.abs(array - min(self.plot_volt[0:self.n_samples])).argmin())  # Find the index where the plot_volt value is the minimum
            self.inf1.setPos(self.plot_wl[index1])  # Make the line1 in max point
            self.inf2.setPos(self.plot_wl[index2])  # Make the line1 in min point
            inf1_txt = str('MAX=\nλ={0:.2f}Å\n'.format(self.plot_wl[index1])) + str('V={0:.5f}volt'.format(self.plot_volt[index1]))
            inf2_txt = str('MIN=\nλ={0:.2f}Å\n'.format(self.plot_wl[index2])) + str('V={0:.5f}volt'.format(self.plot_volt[index2]))
            # self.inf1.setFormat(inf1_txt)
            self.inf1.label.setFormat(inf1_txt)
            self.inf2.label.setFormat(inf2_txt)
    def set_infinite_lines(self, enable):
        if enable:
            self.find_lines_min_max()
            self.graphicsView.addItem(self.inf1)
            self.graphicsView.addItem(self.inf2)
            self.cursors_butt.setEnabled(True)
        else:
            self.graphicsView.removeItem(self.inf1)
            self.graphicsView.removeItem(self.inf2)
            self.cursors_butt.setEnabled(False)

    def line1_moved(self):
        array = np.asarray(self.plot_wl[0:self.n_samples])
        index1 = (np.abs(array - self.inf1.value()).argmin())  # Find the index where the infinite line crosses the plot_wl
        self.inf1.setPos(self.plot_wl[index1])
        inf1_txt = str('λ={0:.2f}Å\n'.format(self.plot_wl[index1])) + str('V={0:.5f}volt'.format(self.plot_volt[index1]))
        self.inf1.label.setFormat(inf1_txt)
    def line2_moved(self):
        array = np.asarray(self.plot_wl[0:self.n_samples])
        index2 = (np.abs(array - self.inf2.value()).argmin()) # Find the index where the infinite line crosses the plot_wl
        self.inf2.setPos(self.plot_wl[index2])
        inf2_txt = str('λ={0:.2f}Å\n'.format(self.plot_wl[index2])) + str('V={0:.5f}volt'.format(self.plot_volt[index2]))
        self.inf2.label.setFormat(inf2_txt)

    def varsetup(self):
        print("meas_speed0 =" + str(self.scan.meas_speed))
        # set standard values
        self.startwl_val.setText(str(self.scan.start_wl))
        self.stopwl_val.setText(str(self.scan.stop_wl))
        #self.meas_speed_val.setText(str(self.meas_speed))
        self.meas_res_val.setText(str(self.scan.resolution))
        # disable some buttons
        self.stop_butt.setEnabled(False)
        self.save_butt.setEnabled(False)
        self.sig_volt_val.setReadOnly(True)
        self.sig_volt_slider.setEnabled(False)

        if not self.ser_lib.findserial():  # No device found or it is busy
            # create message box
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            # msg.setText("No devices found, please check connections!\n\n Press <OK> to Continue or <Cancel> to Close the application\n")
            msg.setText("No devices found, please check connections!")
            # msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            msg.setStandardButtons(QMessageBox.Ok)
            retval = msg.exec_()  # returned value from msgbox
            if retval == QMessageBox.Cancel:
                self.close()
            else:
                self.curwl = 0
            return False
        else:
            self.ser_lib.setdebug(0)  # disable debug mode
            if not self.ser_lib.getstatus():  # get current status, if status is not OK
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText(self.ser_lib.error_msg)  # Shows the error message
                msg.setStandardButtons(QMessageBox.Ok)
                retval = msg.exec_()
                # return False
            self.curwl = self.ser_lib.getwl()  # get current wavelength Å
            self.pmt_box_val.blockSignals(True)
            self.pmt_slider_val.blockSignals(True)
            self.scan.pmt_val = self.ser_lib.getpmt()  # get current pmt_value
            pmt_val_min = self.ser_lib.getpmt_min()  # get min pmt_value
            pmt_val_max = self.ser_lib.getpmt_max()  # get max pmt_value
            self.pmt_box_val.setMinimum(pmt_val_min)
            self.pmt_slider_val.setMinimum(pmt_val_min)
            self.pmt_box_val.setMaximum(pmt_val_max)
            self.pmt_slider_val.setMaximum(pmt_val_max)
            self.pmt_box_val.setValue(self.scan.pmt_val)
            self.pmt_slider_val.setValue(self.scan.pmt_val)
            self.pmt_box_val.blockSignals(False)
            self.pmt_slider_val.blockSignals(False)
            #self.ser_lib.set_amp_mode("AUTO")  # Set amplifier mode to AUTO
            #self.sig_amp_box.setCurrentText("AUTO")  # Set amplifier mode to AUTO
            self.ser_lib.set_amp_mode(self.scan.amp_gain)  # Set amplifier mode
            self.sig_amp_box.setCurrentText(self.scan.amp_gain) # Set amplifier mode text
            ret, f_cutoff = self.ser_lib.set_filter_mode(self.scan.filter_mode)  # Set filter mode to OFF
            if ret:
                self.scan.filter_cutoff = f_cutoff
                self.filter_val.setText(self.scan.filter_cutoff)
            #self.meas_speed_slider_val.setMinimum(int(self.ser_lib.getspeed_min() * 100))
            #self.meas_speed_slider_val.setMaximum(int(self.ser_lib.getspeed_max() * 100))
            self.meas_speed_slider_val.blockSignals(True)  # block signal so the value change trigger is not executed
            self.meas_speed_slider_val.setMinimum(int(np.log10(self.ser_lib.getspeed_min() * 1000) * 1000)) #set value in log scale
            self.meas_speed_slider_val.setMaximum(int(np.log10(self.ser_lib.getspeed_max() * 1000) * 1000)) #set value in log scale
            self.meas_speed_slider_val.setValue(int(np.log10(self.scan.meas_speed * 1000) * 1000))  #set value in log scale
            self.meas_speed_slider_val.blockSignals(False)
            self.meas_speed_val.blockSignals(True) # block signal so the value change trigger is not executed
            self.meas_speed_val.setText(str(self.scan.meas_speed))
            self.meas_speed_val.blockSignals(False)

            # self.ser_lib.setpmt_offset(125)
        self.currwl_val.setText(str(self.curwl))
        print("self.curwl = {}".format(self.curwl))
        self.calc_meas_dur()
        self.status_text.setText("READY")
        return True

    # define what happens on closeEvent
    def closeEvent(self, event):
        if self.meas_status == 0:  # is in standby status
            if not self.saved:
                self.msgbox()
            reply = QMessageBox.question(self, 'Window Close', 'Are you sure you want to close window?',
                                         QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.thread_display.requestInterruption()
                self.thread_display.quit()
                self.thread_display.wait()
                self.thread_display.deleteLater()
                timeout = 1
                time.sleep(timeout)  # wait so that the last iteration of the loop finishes
                print("Timeout="+ str(timeout))
                while not self.ser_lib.savewl():
                    print("wait to save data")
                    time.sleep(0.5)
                self.ser_lib.stopserial()
                event.accept()
                #self.close()
            else:
                event.ignore()
        else:
            reply = QMessageBox.question(self, 'Window Close', 'A Task is running, do you want to Stop?',
                                         QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.stop_msm = True
            event.ignore()

    def params_changed(self):
        # print("PARAMETER_CHANGED")
        # set variables for Å
        try:
            self.scan.start_wl = float(self.startwl_val.text())
            self.scan.stop_wl = float(self.stopwl_val.text())
        except ValueError:
            self.meas_dur_val.setText("N/A")
            pass
        else:
            self.calc_meas_dur()

    # calculate measurment duration
    def calc_meas_dur(self):
        self.scan.resolution = self.ser_lib.get_res(self.scan.meas_speed)
        self.meas_res_val.setText(str(self.scan.resolution))
        self.meas_duration = self.ser_lib.getmsm_dur(self.scan.start_wl, self.scan.stop_wl, self.scan.meas_speed)
        duration = str(datetime.timedelta(seconds=self.meas_duration))  # convert from seconds to hh:mm:ss
        self.meas_dur_val.setText(duration)

    def meas_speed_slider_val_changed(self):
        #self.meas_speed = self.meas_speed_slider_val.value() / 100
        #print ("meas_speed =" +str(self.meas_speed_slider_val.value()))
        slide_val= pow(10,self.meas_speed_slider_val.value()/1000)/1000
        if slide_val > 10:
            self.scan.meas_speed = round(slide_val,-1) #step is 10
        elif slide_val > 1:
            self.scan.meas_speed = round(slide_val,0) #step is 1
        elif slide_val > 0.1:
            self.scan.meas_speed = round(slide_val, 1) #step is 0.1
        else:
            self.scan.meas_speed = round(slide_val, 2) #step is 0.01

        self.meas_speed_val.setText(str(self.scan.meas_speed))
        self.calc_meas_dur()

    def meas_speed_val_changed(self):
        try:
            self.scan.meas_speed = float(self.meas_speed_val.text())
        except ValueError:
            self.meas_dur_val.setText("N/A")
            pass
        else:
            if self.scan.meas_speed > 0.0:
                self.meas_speed_slider_val.setValue(int(np.log10(self.scan.meas_speed * 1000) * 1000)) #set value in log scale
                self.calc_meas_dur()

    def pmt_slider_val_changed(self):
        self.scan.pmt_val = self.pmt_slider_val.value()
        self.pmt_box_val.setValue(self.scan.pmt_val)
        self.ser_lib.setpmt(self.scan.pmt_val)

    def pmt_box_val_changed(self):
        self.scan.pmt_val = self.pmt_box_val.value()
        self.pmt_slider_val.setValue(self.scan.pmt_val)
        self.ser_lib.setpmt(self.scan.pmt_val)

    def amp_changed(self):
        self.ser_lib.set_amp_mode(self.sig_amp_box.currentText())  # "AUTO", "1x" , "16x", "32x" or "64x"
        print("AMP mode selected:" + self.sig_amp_box.currentText())

    def filter_changed(self):
        ret , f_cutoff = self.ser_lib.set_filter_mode(self.filter_box.currentText())  # "OFF" or "FAST" or "SLOW"
        if ret:
            self.scan.filter_mode = self.filter_box.currentText()
            self.scan.filter_cutoff = f_cutoff
            self.filter_val.setText(f_cutoff)
            print("FILTER mode selected:" + self.filter_box.currentText())

    def stopbutton(self):
        print("STOP pressed")
        self.stop_msm = True

    def show_new_data(self, color='w'):  # that allows to update and display the graph, default color 'white'
        #self.data_curve.setData(self.plot_wl[-51:], self.plot_volt[-51:],clear=False) #display the last 50 points
        #self.data_curve.setData(self.plot_wl, self.plot_volt, clear=True)  # display the last 50 points
        self.data_curve.setData(self.plot_wl[0:self.n_samples], self.plot_volt[0:self.n_samples], clear=True, pen=color)  # display
        #print("n_samples=" + str(self.n_samples))
        #self.data_curve.setData(self.plot_wl, self.plot_volt, clear=True, clickable=True)
        #self.graphicsView.setRange(xRange=[self.start_wl, self.curwl + 5 * self.resolution], yRange=[0, 10])
        self.graphicsView.setRange(xRange=[self.scan.start_wl, self.curwl + 5 * self.scan.resolution])
        #self.graphicsView.setAutoVisible(x=False, y=True)
        #self.graphicsView.enableAutoRange(x=False, y=True) #TODO maybe enable
        #self.graphicsView.setRange(xRange=[self.start_wl, self.stop_wl],yRange=[0, 10])

    def graph_thread(self):
        while self.inmsmloop:
            self.show_new_data()
            time.sleep(0.1)  #small delay so it does not refresh so fast
        self.meas_status = 7

    # loop triggered by separated thread
    def display_show(self):
        if self.ser_lib.error_msg != "" or self.timeout >= 0:  # There are Error messages or timeout no passed yet
            if self.timeout >= 0:
                self.timeout -= 1
            else:
                self.error_text.setText(self.ser_lib.error_msg)
                self.error_text.setStyleSheet("background-color: rgb(255, 124, 115);")
                self.timeout = 15
        else:
            self.error_text.setStyleSheet("")
            self.error_text.setText("")
        if self.meas_status == 0:  # get standby vars
            # self.calc_meas_dur()
            buff_pmt_val = self.ser_lib.getpmt()  # get current pm_value
            if self.scan.pmt_val != buff_pmt_val:
                self.scan.pmt_val = buff_pmt_val
                self.pmt_box_val.setValue(self.scan.pmt_val)
                self.pmt_slider_val.setValue(self.scan.pmt_val)
            self.curvolt = self.ser_lib.getvolt()  # Get ADC voltage
            ret, mode, value = self.ser_lib.get_amp_mode()
            if ret: #no error
                self.scan.amp_gain = value
                if mode == "AUTO":
                    self.sig_amp_box.setCurrentText(mode)
                    self.sig_amp_val.setText("[" + value + "]")
                else:
                    self.sig_amp_box.setCurrentText(value)
                    self.sig_amp_val.setText("")
        elif self.meas_status == 1:  # 0-get standby vars; 1-go_to_wavelength start; 2-go_to_wavelength running; 3- measure_start; measure_running
            self.lock_buttons()  # lock buttons
            thread = threading.Thread(target=self.chwl)  # create unique thread
            thread.start()  # start process
            print("thread chwl started")
            self.meas_status = 2
        elif self.meas_status == 2:
            if self.meas_stop_signal == True:
                self.meas_stop_signal = False
                self.unlock_buttons()  # unlock buttons
                self.stop_msm = False
                self.meas_status = 0  # 0-get standby vars; 1-measurement start; 2-measurement-running
        elif self.meas_status == 3:
            self.lock_buttons()  # lock buttons
            self.set_infinite_lines(False)
            self.set_graph_settings()
            # create measuring thread
            thread = threading.Thread(target=self.meas_thread)  # create unique thread
            thread.start()  # start process
            print("meas_thread started")
            self.meas_status = 4
        elif self.meas_status == 4:
            if self.meas_stop_signal == True:
                self.meas_stop_signal = False
                self.unlock_buttons()  # unlock buttons
                self.stop_msm = False
                self.meas_status = 0
        elif self.meas_status == 5:
            #thread2 = threading.Thread(target=self.graph_thread)  # create unique thread
            #thread2.start()  # start process
            #print("graph_thread started")
            self.meas_status = 6
        elif self.meas_status == 6:
            actual_time = time.time() #get the actual time in "s", so that calculates how long the experiment lasts
            duration = str(datetime.timedelta(seconds=int(self.meas_duration -(actual_time-self.meas_start_time) ) ) )  # convert from seconds to hh:mm:ss
            self.meas_dur_val.setText(duration)
            self.show_new_data()
            if self.inmsmloop == False:  # it stopped measuring
                self.unlock_buttons()  # unlock buttons
                self.stop_msm = False
                self.meas_status = 0
                self.calc_meas_dur() #Calculate new measurement duration
                self.set_infinite_lines(True)
                self.graphicsView.setLimits(xMin=self.scan.start_wl, xMax=self.curwl + 5 * self.scan.resolution)
                self.graphicsView.setAutoVisible(x=False, y=True)
                self.graphicsView.enableAutoRange(x=False, y=True)
        self.currwl_val.setText(str(self.curwl))
        self.sig_volt_val.setText(str(self.curvolt))
        self.sig_volt_slider.setValue(int(self.curvolt * 10))

    # changes unit
    def unit_changed(self):
        if self.wl_unit_box.currentIndex() == 1: #Index 1 is Nanometer
            self.scan.wavelength_unit = "nm"
            self.gowl_text.setText("Go to wavelength (nm)")
            self.startwl_text.setText("Wavelength start (nm)")
            self.stopwl_text.setText("Wavelength stop (nm)")
            self.curwl_text.setText("Actual wavelength (nm)")
            self.meas_speed_text.setText("Speed (nm/s)")
            self.meas_res_text.setText("Resolution (nm)")
            self.graphicsView.setLabel('bottom', "Wavelength (nm)")

            if len(self.startwl_val.text()) > 0:
                val = round(float(self.startwl_val.text()) / 10, 3)
                self.startwl_val.setText(str(val))
            if len(self.stopwl_val.text()) > 0:
                val = round(float(self.stopwl_val.text()) / 10, 3)
                self.stopwl_val.setText(str(val))
            if len(self.meas_speed_val.text()) > 0:
                val = round(float(self.meas_speed_val.text()) / 10, 3)
                self.meas_speed_val.setText(str(val))
            if len(self.meas_res_val.text()) > 0:
                val = round(float(self.meas_res_val.text()) / 10, 3)
                self.meas_res_val.setText(str(val))
            if len(self.gowl_val.text()) > 0:
                val = round(float(self.gowl_val.text()) / 10, 3)
                self.gowl_val.setText(str(val))
            self.curwl = round(float(self.curwl) / 10, 3)
            self.factor_mv = 500  # for calculating wl

        elif self.wl_unit_box.currentIndex() == 0: #Index 0 is Ångström:
            self.scan.wavelength_unit = "Å"
            self.gowl_text.setText("Go to wavelength (Å)")
            self.startwl_text.setText("Wavelength start (Å)")
            self.stopwl_text.setText("Wavelength stop (Å)")
            self.curwl_text.setText("Actual wavelength (Å)")
            self.meas_speed_text.setText("Speed (Å/s)")
            self.meas_res_text.setText("Resolution (Å)")
            self.graphicsView.setLabel('bottom', "Wavelength (Å)")
            if len(self.startwl_val.text()) > 0:
                self.startwl_val.setText(str(float(self.startwl_val.text()) * 10))
            if len(self.stopwl_val.text()) > 0:
                self.stopwl_val.setText(str(float(self.stopwl_val.text()) * 10))
            if len(self.meas_speed_val.text()) > 0:
                self.meas_speed_val.setText(str(float(self.meas_speed_val.text()) * 10))
            if len(self.meas_res_val.text()) > 0:
                self.meas_res_val.setText(str(float(self.meas_res_val.text()) * 10))
            if len(self.gowl_val.text()) > 0:
                self.gowl_val.setText(str(float(self.gowl_val.text()) * 10))
            self.curwl = round(float(self.curwl) * 10, 3)
            self.factor_mv = 50  # for calculating wl

    # def thread_move_to_wl(self):
    def move_to_wl(self):
        # check input
        if len(self.currwl_val.text()) > 0 and len(self.gowl_val.text()) > 0:
            try:
                # self.curwl = self.currwl_val.text()
                goto_wl = self.gowl_val.text()
                # self.curwl = float(curwl)
                goto_wl = float(goto_wl)
            except ValueError:
                # open popup
                warning = QMessageBox()
                warning.setIcon(QMessageBox.Warning)
                warning.setText("Please enter only valid numbers!")
                warning.setStandardButtons(QMessageBox.Ok)
                # self.save_butt.setEnabled(False)
                warning.exec_()
                return
            if self.ser_lib.gowl(goto_wl) == False:  # Check if there is an error when moving motor
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText(self.ser_lib.error_msg)
                msg.setStandardButtons(QMessageBox.Ok)
                retval = msg.exec_()
                return
            self.meas_status = 1  # 0-get standby vars; 1-go_to_wavelength start; 2-measurement-running

    # start measurment
    def startmsm(self):
        if self.ser_lib.error_msg != "":  # There are Error messages
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText(self.ser_lib.error_msg)
            msg.setStandardButtons(QMessageBox.Ok)
            retval = msg.exec_()
            return
        if self.saved == True:  # check if data from previous measurement was saved
            self.meas_status = 3  # 0-get standby vars; 1-go_to_wavelength start; 2-goto running; 3-start measurement
        else:  # If not saved
            self.msgbox()
            self.startmsm()

    # change the motor position
    def chwl(self): # TODO:check if is A or nm
        self.curwl = self.ser_lib.getwl()  # get current wavelength maybe already moved
        while self.ser_lib.is_motor_running():
            if self.stop_msm == True:  ##check if Stop button was pressed
                self.ser_lib.stop() # send stop to motor
            time.sleep(0.2)  # seconds
            self.curvolt = self.ser_lib.getvolt()  # Get ADC voltage
            self.curwl = self.ser_lib.getwl()  # get current wavelength
        self.meas_stop_signal = True
        return

    # measure thread: allows to make a scan and sava the data in variables
    def meas_thread(self):
        self.plot_volt = [0]*self.datasize
        self.plot_wl = [0]*self.datasize
        self.n_samples = 0
        print("current_wl:" + str(self.curwl) + "start_wl:" + str(self.scan.start_wl) + " Stop_wl" + str(self.scan.stop_wl))
        print("resolution=" + str(self.scan.resolution))
        stop_trigg = False
        if self.ser_lib.start_scan(self.scan.start_wl, self.scan.stop_wl, self.scan.meas_speed):
            self.saveScan = copy(self.scan) #the saveScan should hold the scan parameters until a save is done
            ret, msg, wl, volt = self.ser_lib.check_scan_status()
            while ret or stop_trigg == True:
                if self.stop_msm == True and stop_trigg == False:  ##check if Stop button was pressed
                    stop_trigg = True
                    self.ser_lib.stop()  # send stop to the motor
                if msg == "moving_to_start":
                    self.curwl = wl
                    self.curvolt = volt
                elif msg == "scanning":
                    break
                ret, msg, wl, volt = self.ser_lib.check_scan_status()
                if not ret:  # if status is invalid
                    if self.ser_lib.stop() == True:  # send stop and check if motor already stopped
                        self.curwl = self.ser_lib.getwl()  # get current wavelength
                        self.meas_stop_signal = True
                        return

            stop_trigg = False
            self.saved = False
            self.inmsmloop = True  # is in measurement loop
            self.meas_start_time = time.time() #get time at starting of measurement
            self.meas_status = 5
            ret, wl, volt = self.ser_lib.scan_new_val()
            while True:
                if self.stop_msm == True and stop_trigg == False:  ##check if Stop button was pressed
                    self.ser_lib.stop(False)  # send stop
                    stop_trigg = True
                if ret:  # if new scan is valid
                    self.curwl = wl
                    self.curvolt = volt
                    self.plot_wl[self.n_samples] = wl
                    self.plot_volt[self.n_samples] = volt
                    self.n_samples +=1
                else:
                    if wl == "scan=reads_done":
                        print("Total Samples="+str(self.n_samples))
                        break
                    print("SCAN is invalid="+str(wl))
                    print("Last valid curwl="+str(self.curwl))
                ret, wl, volt = self.ser_lib.scan_new_val()
            self.curwl = self.ser_lib.getwl()  # get current wavelength
            self.inmsmloop = False  # is not in measurement loop
        else:  # if an error occoured
            self.curwl = self.ser_lib.getwl()  # get current wavelength
            self.meas_stop_signal = True

    def save(self):
        dlg = QtWidgets.QInputDialog(self)
        dlg.setInputMode(QtWidgets.QInputDialog.TextInput)
        dlg.setLabelText("Write a Description for your measurement")
        dlg.resize(300, 200)
        dlg.exec_()

        savedir = os.path.realpath(os.path.expanduser('~/Desktop/MEASUREMENTS'))
        pathlib.Path(savedir).mkdir(parents=True, exist_ok=True)  # Make directory if does not exist
        filename = tkinter.filedialog.asksaveasfilename(initialdir=savedir, defaultextension=".dat",
                                                        title="Write a filename for you measurement",
                                                        filetypes=[("data files", '*.dat')])  # get .dat filename
        if len(filename) > 0:  # check if a file was entered
            if not filename.endswith('.dat'):
                filename += '.dat'
            filenamepicture = filename[:-4] + ".png"  # remove .dat and add .png
            # Check if there is a file with the same filename
            if os.path.isfile(filenamepicture) == True:
                filenamepicture_old = filenamepicture
                filenamepicture = filenamepicture[:-4] + "_1.png"  # remove .dat and add .png
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText(
                    "There is a already a File: " + filenamepicture_old + "\nA new File " + filenamepicture + " was created")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()  # returned value from msgbox
            print("filename=" + filename)
            print("filenamepicture=" + filenamepicture)

            # create file contents
            fileinput = "##Filename " + filename + \
                        "\n##Description: " + dlg.textValue() + \
                        "\n##Wavelength Unit: " + self.saveScan.wavelength_unit + \
                        "\n##Wavelength Start: " + str(self.saveScan.start_wl) + " " + self.saveScan.wavelength_unit + \
                        "\n##Wavelength Stop: " + str(self.saveScan.stop_wl) + " " + self.saveScan.wavelength_unit + \
                        "\n##Measurement Speed: " + str(self.saveScan.meas_speed) + " " + self.saveScan.wavelength_unit + "/s" + \
                        "\n##Resolution: " + str(self.saveScan.resolution) + " " + self.saveScan.wavelength_unit + \
                        "\n##Photomultiplier Supply: " + str(self.saveScan.pmt_val) + " V" + \
                        "\n##Signal Filter: " + self.saveScan.filter_mode + self.saveScan.filter_cutoff + \
                        "\n##Amplifier Gain: " + self.saveScan.amp_gain + \
                        "\n##x-data  y-data \n"

            for sample in range(self.n_samples):
                line = str(self.plot_wl[sample]) + "  " + str(self.plot_volt[sample]) + "\n"
                fileinput = fileinput + ''.join(line)  #Huge amount of time saved by using join, instead of using + !!!

            fileinput = fileinput + "##END##"
            with open(filename, 'w') as file:
                file.write(fileinput)
            # crate plot to save image
            # plot = pg.PlotWidget()
            # plt = pg.plot(self.plot_wl, self.plot_volt)
            # exporter = pg.exporters.ImageExporter(plt.plotItem)
            # self.graphicsView.plot(self.plot_wl, self.plot_volt, clear=True)  # , clear=True #Plot graph
            self.graphicsView.setBackground('w')
            self.show_new_data('black')
            exporter = pg.exporters.ImageExporter(self.graphicsView.plotItem)
            exporter.parameters()['width'] = 1000  # width of image
            exporter.export(filenamepicture)
            self.graphicsView.setBackground('black')
            self.show_new_data('white')
            self.status_text.setText("Files have been saved")
            self.save_butt.setEnabled(False)
            self.saved = True
            print("Files have been saved")
        else:
            self.status_text.setText("Files were Not Saved!")
            print("Files were Not Saved!")

    def msgbox(self):
        # create message box
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Measurement Not saved!\nDo you want to save it?")
        msg.setStandardButtons(QMessageBox.Save | QMessageBox.Discard)
        retval = msg.exec_()  # returned value from msgbox
        if retval == QMessageBox.Save:
            self.save()
        elif retval == QMessageBox.Discard:
            self.saved = True

    def setnewwl(self):
        # create popup
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Are you sure the monochromator is in the wrong wavelength?")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        retval = msg.exec_()  # returned value from msgbox

        if retval == QMessageBox.Yes:
            # create input popup
            dlg = QtWidgets.QInputDialog(self)
            dlg.setInputMode(QtWidgets.QInputDialog.TextInput)
            dlg.setLabelText("Set new wavelength (" + self.scan.wavelength_unit + "):")
            dlg.resize(300, 200)
            ok = dlg.exec_()
            if ok:
                try:
                    new_set_val = float(dlg.textValue())
                except ValueError:
                    # open popup
                    warning = QMessageBox()
                    warning.setIcon(QMessageBox.Warning)
                    warning.setText("Please enter only valid numbers!")
                    warning.setStandardButtons(QMessageBox.Ok)
                    # self.save_butt.setEnabled(False)
                    warning.exec_()
                    return
                if self.ser_lib.setwl(new_set_val):  #if values were correctly saved
                    self.curwl = new_set_val
                else:
                    warning = QMessageBox()
                    warning.setIcon(QMessageBox.Warning)
                    warning.setText(self.ser_lib.error_msg)  # Shows the error message
                    warning.setStandardButtons(QMessageBox.Ok)
                    warning.exec_()
                    
    def wavelength_to_rgb(self,wavelength, gamma=0.8):
        ''' taken from http://www.noah.org/wiki/Wavelength_to_RGB_in_Python
        This converts a given wavelength of light to an
        approximate RGB color value. The wavelength must be given
        in nanometers in the range from 380 nm through 750 nm
        (789 THz through 400 THz).

        Based on code by Dan Bruton
        http://www.physics.sfasu.edu/astro/color/spectra.html
        Additionally alpha value set to 0.5 outside range
        '''
        wavelength = float(wavelength)
        if wavelength >= 380 and wavelength <= 750:
            A = 1.
        else:
            A = 0.5
        if wavelength < 380:
            wavelength = 380.
        if wavelength > 750:
            wavelength = 750.
        if 380 <= wavelength <= 440:
            attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380)
            R = ((-(wavelength - 440) / (440 - 380)) * attenuation) ** gamma
            G = 0.0
            B = (1.0 * attenuation) ** gamma
        elif 440 <= wavelength <= 490:
            R = 0.0
            G = ((wavelength - 440) / (490 - 440)) ** gamma
            B = 1.0
        elif 490 <= wavelength <= 510:
            R = 0.0
            G = 1.0
            B = (-(wavelength - 510) / (510 - 490)) ** gamma
        elif 510 <= wavelength <= 580:
            R = ((wavelength - 510) / (580 - 510)) ** gamma
            G = 1.0
            B = 0.0
        elif 580 <= wavelength <= 645:
            R = 1.0
            G = (-(wavelength - 645) / (645 - 580)) ** gamma
            B = 0.0
        elif 645 <= wavelength <= 750:
            attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
            R = (1.0 * attenuation) ** gamma
            G = 0.0
            B = 0.0
        else:
            R = 0.0
            G = 0.0
            B = 0.0
        return (255*R, 255*G, 255*B, 255*A)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog_new()
    ui.show()
    sys.exit(app.exec_())
