import serial
import time
import serial.tools.list_ports


class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        #self.buf = None
        self.s = s

    def readline(self):
        start = time.time()
        i = self.buf.find(b"\n")
        #i = self.buf.find(b"\r\n")
        #print("found_EOL=" + str(i))
        if i >= 0:
            r = self.buf[:i + 1]
            self.buf = self.buf[i + 1:]
            r_str = r.decode('utf-8')
            r_str = r_str.strip()
            return r_str
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            i = data.find(b"\n")
            #i = data.find(b"\r\n")
            #print("found_EOL=" + str(i))
            if i >= 0:
                r = self.buf + data[:i + 1]
                self.buf[0:] = data[i + 1:]
                r_str = r.decode('utf-8')
                r_str =r_str.strip()
                return r_str
            else:
                self.buf.extend(data)
                if len(self.buf) == 0:
                    return str("")


# Library class for all communications with ESP32
class SerialLib(object):

    def __init__(self, parent=None):
        self.ser = serial.Serial()
        # self.ser.port == 1000  # default number, not an actual port
        # self.ser.baudrate = 57600
        self.ser.baudrate = 115200
        self.ser.timeout = 1.0
        #self.ser.timeout = 0.5
        self.error_msg = ""
        self.debug = False
        self.debug2 = True
        self.status_msg = ""
        self.reader = ReadLine(self.ser)

    # Automatically find port number of ESP32
    def findserial(self):
        # self.ser.port = '1000'
        ports = []
        print("Scan Serial Ports")
        ports = list(serial.tools.list_ports.comports())
        # ports= serial.tools.list_ports.comports()
        for p in ports:
            print(p.device)
            print(p.manufacturer)
            print(p.name)
            print(p.serial_number)
            print(p.location)
            print(p.description)
            print(p.product)
            print(p.interface)

        print(len(ports), 'ports found')

        if not ports:
            print("No devices on ports found, please restart application")
        # scan ports for Ardunio
        # for a in range(len(ports)):
        for port in ports:
            # if 'Arduino' in str(port.manufacturer):  # Linux adaptation
            if 'Silicon Labs' in str(port.manufacturer):  # Linux adaptation
                # if 'Arduino Uno' in str(ports[a]):
                print("Silicon Labs Port found")
                self.ser.port = port.device
                # self.ser.baudrate = 115200
                try:
                    self.ser.open()
                except:  # Could not open serial port
                    return False
                self.ser.write(str.encode('version\r\n'))
                for i in range(6):  # run 6 loops or until the version is correct
                    answer = self.reader.readline()
                    print(answer)
                    if answer == "STEP MOTOR Controller THR1000 V1.0":
                        print("Device Connection established")
                        return True  # if finds a com port
                print("Device Found but version not recognized")
                self.ser.close()
                return False  # if does not find a com port

    # stop receiving data from ESP32, flush buffer and close Com
    def stopserial(self):
        if (self.ser.isOpen() == True):
            # self.ser.write(str.encode('s'))
            self.ser.flushInput()
            time.sleep(.1)
            self.ser.flushOutput()
            time.sleep(.1)
            self.ser.close()
            print("Serial Com Port is stopping")

    def getwl(self):
        for x in range(6):
            self.ser.write(str.encode('getwl\r\n'))
            if self.debug: print("Try to> getwl:")
            answer = self.reader.readline()
            if self.debug: print(answer)
            if answer.startswith("getwl=") and answer.endswith("=G"): #=G is like checksum
                answer = answer.split('=')
                if self.debug: print(answer[1])
                return float(answer[1])  ##return with 2 decimals
            elif answer.startswith("ERROR:"):
                self.error_msg = self.status_msg + answer
                return -1
            time.sleep(0.2) #0.2s to try again
        return -1

    def setwl(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('setwl=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> setwl:" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("setwl="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if float(answer[1]) == set_value:
                if self.debug: print("Value correctly saved")
                return True
        elif answer.startswith("ERROR:"):
            self.error_msg = self.status_msg + answer
        else:
            self.error_msg = self.status_msg + "UNKNOWN ERROR"
        return False

    def gowl(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('gowl=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> gowl:" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("gowl="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if float(answer[1]) == set_value:
                if self.debug: print("Value correctly sent")
                return True
        elif answer.startswith("ERROR:"):
            self.error_msg = self.status_msg + answer
        else:
            self.error_msg = self.status_msg + "UNKNOWN ERROR"
        if self.debug: print(answer)
        return False

    def stop(self, wait_for_reply=True):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('stop\r\n'))
        if self.debug: print("Try to> stop:")
        if not wait_for_reply:
            return False
        for x in range(6):
            answer = self.reader.readline()
            if self.debug: print(answer)
            if answer.startswith("stop="):
                answer = answer.split('=')
                if self.debug: print(answer[1])
                if answer[1] == "done":
                    if self.debug: print("Motor is stopped")
                    return True
                return False
            elif answer.startswith("ERROR:"):
                self.error_msg = self.status_msg + answer
                return False
        #time.sleep(0.2) #0.2s to try again
        return False

    def is_motor_running(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('get_mot_state\r\n'))
        if self.debug: print("Try to> get_mot_state:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("get_mot_state="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if answer[1] == "run":
                if self.debug: print("Motor is still running")
                return True
            elif answer[1] == "stop":
                if self.debug: print("Motor is stopped")
                return False
        return True #we assume motor is still running if there is an error

    def savewl(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('savewl\r\n'))
        if self.debug: print("Try to> savewl:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("savewl="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return True
        return False

    def getvolt(self):
        self.error_msg = self.status_msg
        self.getstatus()  # always get status
        self.ser.write(str.encode('getvolt\r\n'))
        if self.debug: print("Try to> getvolt:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getvolt="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            try:
                return float(answer[1]) #return voltage
            except ValueError:
                return 0
        return 0

    def setdebug(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('debug=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> debug=:" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("debug="):
            answer = answer.split('=')
            if self.debug2: print(answer[1])
            if float(answer[1]) == set_value:
                if self.debug: print("Value correctly set")
                return True
        return False

    def getpmt(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getpmt\r\n'))
        if self.debug: print("Try to> getpmt:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getpmt="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return int(answer[1])
        return 0

    def getpmt_min(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getpmt_min\r\n'))
        if self.debug: print("Try to> getpmt_min:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getpmt_min="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return int(answer[1])
        return 0

    def getpmt_max(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getpmt_max\r\n'))
        if self.debug: print("Try to> getpmt_max:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getpmt_max="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return int(answer[1])
        return 0

    def setpmt(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('setpmt=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> setpmt:" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("setpmt="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if float(answer[1]) == set_value:
                if self.debug: print("Value correctly set")
                return True
        return False

    def setpmt_offset(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('setpmt_offset=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> setpmt_offset:" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("setpmt_offset="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if float(answer[1]) == set_value:
                if self.debug: print("Value correctly set")
                return True
        return False

    def getspeed_min(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getspeed_min\r\n'))
        if self.debug: print("Try to> getspeed_min")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getspeed_min="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return float(answer[1])
        return 0

    def getspeed_max(self):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getspeed_max\r\n'))
        if self.debug: print("Try to> getspeed_max")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getspeed_max="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            return float(answer[1])
        return 0

    def set_amp_mode(self, set_value):
        self.error_msg = self.status_msg
        self.ser.write(str.encode('set_amp_mode=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> set_amp_mode=" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("set_amp_mode="):
            answer = answer.split('=')
            if self.debug: print(answer[1])
            if answer[1] == set_value:
                if self.debug: print("Value correctly sent")
                return True
        return False

    def get_amp_mode(self):    #example of answer: get_amp_mode=AUTO=;VAL=1x=G (=G is like checksum)
        self.error_msg = self.status_msg
        self.ser.write(str.encode('get_amp_mode\r\n'))
        if self.debug: print("Try to> get_amp_mode")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("get_amp_mode=") and answer.endswith("=G"):
            answer = answer.split('=')
            if self.debug: print(answer)
            if self.debug: print("mode=" + answer[1])
            if self.debug: print("value=" + answer[3])
            return True, answer[1], answer[3]
        return False, "", ""

    def set_filter_mode(self, set_value):  #example of answer set_filter_mode=FAST;fc=2kHz=G
        self.error_msg = self.status_msg
        self.ser.write(str.encode('set_filter_mode=' + str(set_value) + '\r\n'))
        if self.debug: print("Try to> set_filter_mode=" + str(set_value))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("set_filter_mode=") and answer.endswith("=G"):
            answer = answer.split('=')
            if self.debug: print(answer)
            if self.debug: print("mode=" + answer[1])
            if self.debug: print("f_cutoff=" + answer[3])
            if answer[1] == set_value:
                if self.debug: print("Value correctly sent")
                return True, answer[3]
        return False, ""

    def start_scan(self, start_value,stop_value,speed):  #scan=2000;2500;70 Make a scan form 2000Å to 2500Å, with speed 70Å/s
        self.error_msg = self.status_msg
        self.ser.write(str.encode('scan=' + str(start_value) + ';' + str(stop_value)+ ';' + str(speed) + '\r\n'))
        if self.debug2: print("Try to> scan=" + str(start_value) + ';' + str(stop_value) + ';' + str(speed))
        answer = self.reader.readline()
        if self.debug2: print(answer)
        if answer.startswith("scan=starting"):
            return True
        elif answer.startswith("ERROR:"):
            self.error_msg = self.status_msg + answer
        return False

    def check_scan_status(self):
        answer = self.reader.readline()  # example answer-> scan=moving_to_start=;wl=2000.00=;volt=0.91104=G (=G is like checksum)
        if answer.startswith("scan=moving_to_start") and answer.endswith("=G"):
            answer = answer.split('=')
            if self.debug: print(answer)
            if self.debug: print("msg =" + answer[1])
            if self.debug: print("wl =" + answer[3])
            if self.debug: print("volt =" + answer[5])
            return True, answer[1], float(answer[3]), float(answer[5])
        elif answer.startswith("scan=scanning"):
            answer = answer.split('=')
            if self.debug: print(answer)
            return True, answer[1], "", ""
        return False, "", "", ""

    def scan_new_val(self):
        answer = self.reader.readline()  #example answer-> scans=1=;wl=2000.00=;volt=0.91104=G (=G is like checksum)
        if answer.startswith("scans=") and answer.endswith("=G"):
            answer = answer.split('=')
            if self.debug: print(answer)
            if self.debug: print("id =" + answer[1])
            if self.debug: print("wl =" + answer[3])
            if self.debug: print("volt =" + answer[5])
            return True, float(answer[3]), float(answer[5])
        elif answer.startswith("stop="):
            answer = str(answer)
            if self.debug: print(answer)
            return False, answer, ""
        elif answer.startswith("scan=reads_done="):
            if self.debug: print(answer)
            return False, "scan=reads_done", ""
        print("SCAN invalid_DATA="+str(answer))
        return False, "", ""

    def getmsm_dur(self, start_value,stop_value,speed):  #example getmsm_dur = 2000;2500;10;0.1
        self.error_msg = self.status_msg
        self.ser.write(str.encode('getmsm_dur=' + str(start_value) + ';' + str(stop_value) + ';' + str(speed) +';' + '\r\n'))
        if self.debug: print("Try to> getmsm_dur=" + str(start_value) + ';' + str(stop_value) + ';' + str(speed))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("getmsm_dur="):
            answer = answer.split('=')
            if self.debug: print(answer)
            return float(answer[1])
        return False

    def get_res(self, speed):  #get resolution, for a given speed, example: get_res=70
        self.error_msg = self.status_msg
        self.ser.write(str.encode('get_res=' + str(speed) + '\r\n'))
        if self.debug: print("Try to> get_res=" + str(speed))
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("get_res="):
            answer = answer.split('=')
            if self.debug: print(answer)
            return float(answer[1])
        elif answer.startswith("ERROR:"):
            self.error_msg = self.status_msg + answer
        else:
            self.error_msg = self.status_msg + "UNKNOWN ERROR"
        if self.debug: print(answer)
        return 0

    def getstatus(self):
        self.status_msg = ""
        self.ser.write(str.encode('status\r\n'))
        if self.debug: print("Try to> get status:")
        answer = self.reader.readline()
        if self.debug: print(answer)
        if answer.startswith("status="):
            answer = answer.split('=')
            if self.debug: print(answer)
            if answer[1] == "OK":
                if self.debug: print("STATUS is OK")
                self.status_msg = ""
                self.error_msg = self.status_msg
                return True
            else:
                if self.debug: print("STATUS is NOT OK")
                self.status_msg = answer[1]
                self.error_msg = self.status_msg
                return False
        else:
            self.error_msg = "UNKNOWN ERROR"
        return False
